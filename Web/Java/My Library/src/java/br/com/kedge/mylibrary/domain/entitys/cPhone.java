package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cPhone extends cEntityDomain {

    public cPhone(int id, boolean active, int ddd, int phone){
        super(id, active);

        this.setDdd(ddd);
        this.setPhone(phone);
    }
    private int ddd;
    private int phone;

    public int getDdd() {
        return ddd;
    }

    public void setDdd(int ddd) {
        this.ddd = ddd;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }
}

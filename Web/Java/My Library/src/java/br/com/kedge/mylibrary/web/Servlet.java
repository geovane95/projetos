/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kedge.mylibrary.web;

import br.com.kedge.mylibrary.core.aplication.cResult;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.web.command.iCommand;
import br.com.kedge.mylibrary.web.command.implementation.*;
import br.com.kedge.mylibrary.web.viewhelper.iViewHelper;
import br.com.kedge.mylibrary.web.viewhelper.implementation.cBannerCardViewHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Servlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private static Map<String, iCommand> commands;
    private static Map<String, iViewHelper> viewhelpers;

    public Servlet(){
        commands = new HashMap<String, iCommand>();

        commands.put("create", new cCreateCommand());
        commands.put("drop", new cDropCommand());
        commands.put("retrive", new cRetriveCommand());
        commands.put("view", new cViewCommand());
        commands.put("update", new cUpdateCommand());

        viewhelpers = new HashMap<String, iViewHelper>();

        viewhelpers.put("/mylibrary/CreateBannerCard", new cBannerCardViewHelper());
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doProcessRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doProcessRequest(request, response);
    }

    protected void doProcessRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String uri = request.getRequestURI();

        String operation = request.getParameter("operation");

        iViewHelper vh = viewhelpers.get(uri);

        cEntityDomain entity = vh.getEntity(request);

        iCommand command = commands.get(operation);


        cResult objResult = command.execute(entity);


        vh.setView(objResult, request, response);
    }
}

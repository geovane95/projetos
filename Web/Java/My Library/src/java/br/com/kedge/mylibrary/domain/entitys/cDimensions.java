package br.com.kedge.mylibrary.domain.entitys;

public class cDimensions {

	public cDimensions(double altura, double largura, double peso, double profundidade){
	    this.altura = altura;
	    this.largura = largura;
	    this.peso = peso;
	    this.profundidade = profundidade;
    }
	private double altura;

	private double largura;

	private double peso;

	private double profundidade;

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getLargura() {
		return largura;
	}

	public void setLargura(double largura) {
		this.largura = largura;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getProfundidade() {
		return profundidade;
	}

	public void setProfundidade(double profundidade) {
		this.profundidade = profundidade;
	}
}

package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cPricingGroup extends cEntityDomain {

    public cPricingGroup(int id, boolean active, String name, cPricing objPricing){
        super(id, active);
        this.name = name;
        this.setObjPricing(objPricing);
    }

    private String name;
    private cPricing objPricing;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public cPricing getObjPricing() {
        return objPricing;
    }

    public void setObjPricing(cPricing objPricing) {
        this.objPricing = objPricing;
    }
}

package br.com.kedge.mylibrary.domain.entitys;

public class cStreet {
    public cStreet(){}
    public cStreet(String street, cTypeStreet objTypeStreet){

        this.setStreet(street);
        this.setObjTypeStreet(objTypeStreet);
    }

    private String street;

    private cTypeStreet objTypeStreet;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public cTypeStreet getObjTypeStreet() {
        return objTypeStreet;
    }

    public void setObjTypeStreet(cTypeStreet objTypeStreet) {
        this.objTypeStreet = objTypeStreet;
    }
}

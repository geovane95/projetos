package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cCreditCard extends cEntityDomain {

    public cCreditCard(){}
    public cCreditCard(int id, boolean active, String holder, int number, int monthValid,
                       int yearValid, int codSec, cBannerCard objBannerCard, int user){
        super(id, active);

        this.setNomeCartao(holder);
        this.setNumero(number);
        this.setMesValid(monthValid);
        this.setAnoValid(yearValid);
        this.setCodSeg(codSec);
        this.setObjBannerCard(objBannerCard);
        this.setUser(user);
    }

    private String nomeCartao;
    private int numero;
    private int mesValid;
    private int anoValid;
    private int codSeg;
    private cBannerCard objBannerCard;
    private int user;

    public String getNomeCartao() {
        return nomeCartao;
    }

    public void setNomeCartao(String nomeCartao) {
        this.nomeCartao = nomeCartao;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getMesValid() {
        return mesValid;
    }

    public void setMesValid(int mesValid) {
        this.mesValid = mesValid;
    }

    public int getAnoValid() {
        return anoValid;
    }

    public void setAnoValid(int anoValid) {
        this.anoValid = anoValid;
    }

    public int getCodSeg() {
        return codSeg;
    }

    public void setCodSeg(int codSeg) {
        this.codSeg = codSeg;
    }

    public cBannerCard getObjBannerCard() {
        return objBannerCard;
    }

    public void setObjBannerCard(cBannerCard objBannerCard) {
        this.objBannerCard = objBannerCard;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}

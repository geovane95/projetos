package br.com.kedge.mylibrary.core.aplication;

import br.com.kedge.mylibrary.domain.cEntityDomain;

import java.util.List;

public class cResult extends cEntityAplication{

    private String msg;
	private List<cEntityDomain> entitys;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<cEntityDomain> getEntitys() {
        return entitys;
    }

    public void setEntitys(List<cEntityDomain> entitys) {
        this.entitys = entitys;
    }
}

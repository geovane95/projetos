package br.com.kedge.mylibrary.core.util;

public abstract class acTableModels {
    public final static String[] Authors = {"authors", "id"};
    public final static String[] BannerCards = {"bannercards", "id"};
    public final static String[] Books = {"books", "id"};
    public final static String[] Categories = {"categories", "id"};
    public final static String[] CreditCards = {"creditcard", "id"};
    public final static String[] Dimensions = {"dimensions", "id"};
    public final static String[] Editors = {"editors", "id"};
    public final static String[] Employees = {"employees", "id"};
    public final static String[] LegalPersons = {"legalpersons", "id"};
    public final static String[] NaturalPersons = {"naturalpersons", "id"};
    public final static String[] Phones = {"phones", "id"};
    public final static String[] PricingGroups = {"pricinggroups", "id"};
    public final static String[] TypePhones = {"typephones", "id"};
    public final static String[] TypeAddress = {"typeaddress", "id"};
    public final static String[] Address = {"address", "id"};
    public final static String[] Operations = {"operations", "id"};
    public final static String[] History = {"history", "id"};
}

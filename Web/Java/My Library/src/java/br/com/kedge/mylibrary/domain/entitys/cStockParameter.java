package br.com.kedge.mylibrary.domain.entitys;

public class cStockParameter {

	public cStockParameter(int qtd_min_est, int qtd_max_est){
		this.qtd_min_est = qtd_min_est;
		this.qtd_max_est = qtd_max_est;
	}

	private int qtd_min_est;

	private int qtd_max_est;

	public int getQtd_min_est() {
		return qtd_min_est;
	}

	public void setQtd_min_est(int qtd_min_est) {
		this.qtd_min_est = qtd_min_est;
	}

	public int getQtd_max_est() {
		return qtd_max_est;
	}

	public void setQtd_max_est(int qtd_max_est) {
		this.qtd_max_est = qtd_max_est;
	}
}

package br.com.kedge.mylibrary.core;

import br.com.kedge.mylibrary.core.aplication.cResult;
import br.com.kedge.mylibrary.domain.cEntityDomain;

public interface iFacade {

	public cResult create(cEntityDomain entity);

	public cResult update(cEntityDomain entity);

	public cResult drop(cEntityDomain entity);

	public cResult retrive(cEntityDomain entity);

	public cResult view(cEntityDomain entity);

}

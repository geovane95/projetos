package br.com.kedge.mylibrary.core.implementation.dao;

import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.core.util.acTableModels;
import br.com.kedge.mylibrary.domain.entitys.cAuthor;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class cAuthorsDao extends acJdbcDao{
    protected cAuthorsDao() {
        super(acTableModels.Authors[0], acTableModels.Authors[1]);
    }

    @Override
    public void create(cEntityDomain entity) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        cAuthor objAuthors = (cAuthor) entity;

        try{
            connection.setAutoCommit(false);

            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO ");
            sql.append(acTableModels.Authors[0]);
            sql.append("(id, name, books)");
            sql.append(" VALUES (?, ?, ?)");

            pst = connection.prepareStatement(sql.toString());

            pst.executeUpdate();
            connection.commit();
        }catch (SQLException e){
            try{
                connection.rollback();
            }catch (SQLException se){
                se.printStackTrace();
            }
            e.printStackTrace();
        }

    }

    @Override
    public void update(cEntityDomain entity) throws SQLException {

    }

    @Override
    public List retrive(cEntityDomain entity) throws SQLException {
        return null;
    }
}

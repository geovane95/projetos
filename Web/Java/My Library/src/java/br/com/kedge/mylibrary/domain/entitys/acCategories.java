package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public abstract class acCategories extends cEntityDomain {

	public acCategories(){}
	public acCategories(int id, boolean active, String name){
		super(id, active);
		this.name = name;
	}

	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

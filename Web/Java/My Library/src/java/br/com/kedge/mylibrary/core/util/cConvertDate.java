package br.com.kedge.mylibrary.core.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class cConvertDate {
    public static String convertDateString(Date dtDate){
        SimpleDateFormat formatBra = new SimpleDateFormat("dd/MM/yyyy");
        return (formatBra.format(dtDate));
    }

    public static Date convertStringDate(String stDate) {
        if (stDate == null || stDate.equals("")){
            return null;
        }

        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            date = (java.util.Date)formatter.parse(stDate);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static java.sql.Date convertjDateTosqlDate(){
        java.sql.Date date = null;

        return date;
    }
}

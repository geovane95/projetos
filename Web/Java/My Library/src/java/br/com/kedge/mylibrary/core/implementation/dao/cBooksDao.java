package br.com.kedge.mylibrary.core.implementation.dao;

import br.com.kedge.mylibrary.core.util.acTableModels;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entitys.cBooks;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class cBooksDao extends acJdbcDao {

    public cBooksDao() {
		super(acTableModels.Books[0],acTableModels.Books[1]);
	}

	@Override
	public void create(cEntityDomain entity) {
        openConnection();
        PreparedStatement pst = null;
        cBooks objBooks = (cBooks) entity;

        try {
            connection.setAutoCommit(false);

            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO books(title, year, numpages, synopsis, barcode, cost, value, " +
                    "pricing, user, isbn, edition, qtdminest, qtdmaxest, timerecurrence, qtdminbooks, " +
                    "height, width, length, depth)");
            sql.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            pst = connection.prepareStatement(sql.toString());

            pst.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            e.printStackTrace();
        }
    }

	public void update(cEntityDomain entity) {

	}

	public List<cEntityDomain> retrive(cEntityDomain entity) {
		return null;
	}

}

package br.com.kedge.mylibrary.domain.entitys;

import java.util.Date;

public abstract class acUser extends acPerson {

    public acUser(){}
    public acUser(int id, boolean active, String name, Date dtBeginLife, String email, String password, Date dtReg){
        super(id, active, name, dtBeginLife);

        this.setEmail(email);

        this.setDtReg(dtReg);

        this.setPassword(password);
    }

    private String email;

    private String password;

    private Date dtReg;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDtReg() {
        return dtReg;
    }

    public void setDtReg(Date dtReg) {
        this.dtReg = dtReg;
    }
}

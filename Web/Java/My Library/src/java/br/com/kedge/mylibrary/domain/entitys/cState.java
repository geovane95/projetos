package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cState extends cEntityDomain {

    public cState(){}

    public cState(Integer id, boolean active, String state, cCountry objCountry){
        super(id, active);

        this.setState(state);

        this.setObjCountry(objCountry);
    }

    private String state;

    private cCountry objCountry;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public cCountry getObjCountry() {
        return objCountry;
    }

    public void setObjCountry(cCountry objCountry) {
        this.objCountry = objCountry;
    }
}

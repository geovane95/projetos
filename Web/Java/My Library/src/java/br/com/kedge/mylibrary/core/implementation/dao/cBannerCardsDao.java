package br.com.kedge.mylibrary.core.implementation.dao;

import br.com.kedge.mylibrary.core.util.acTableModels;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entitys.cBannerCard;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class cBannerCardsDao extends acJdbcDao{

    public cBannerCardsDao() {
        super(acTableModels.BannerCards[0], acTableModels.BannerCards[1]);
    }

    @Override
    public void create(cEntityDomain entity){
        openConnection();
        PreparedStatement pst = null;
        cBannerCard objBannerCard = (cBannerCard) entity;

        try{
            connection.setAutoCommit(false);

            StringBuilder sql = new StringBuilder();

            sql.append("INSERT INTO ");
            sql.append(acTableModels.BannerCards[0]);
            sql.append("(banner)");
            sql.append(" VALUES (?)");

            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, objBannerCard.getBanner());

            pst.executeUpdate();

            int id;
            id = 0;
            try (ResultSet rs = pst.getGeneratedKeys()) {
                if (rs.next()) {
                    id = rs.getInt(1);
                }
                objBannerCard.setId(id);
                objBannerCard.setActive(true);
            }catch (Exception e){
                System.out.println("Erro ao obter as chaves geradas na criação de registro");
                e.printStackTrace();
            }
            
            connection.commit();
        }catch (SQLException e){
            try{
                connection.rollback();
            }catch (SQLException se){
                se.printStackTrace();
            }
            e.printStackTrace();
        }finally {
            try{
                pst.close();
                connection.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void update(cEntityDomain entity){
        openConnection();
        PreparedStatement pst = null;
        cBannerCard objBannerCard = (cBannerCard) entity;

        try{
            connection.setAutoCommit(false);

            StringBuilder sql = new StringBuilder();

            sql.append("UPDATE ");
            sql.append(acTableModels.BannerCards[0]);
            sql.append(" SET banner=?, active=?");
            sql.append(" WHERE ");
            sql.append(acTableModels.BannerCards[1]);
            sql.append("=?");

            pst = connection.prepareStatement(sql.toString());
            pst.setString(1, objBannerCard.getBanner());
            pst.setBoolean(2, objBannerCard.isActive());
            pst.setInt(3, objBannerCard.getId());

            pst.executeUpdate();

            connection.commit();
        }catch (SQLException e){
            try{
                connection.rollback();
            }catch (SQLException se){
                se.printStackTrace();
            }
            e.printStackTrace();
        }finally {
            try {
                pst.close();
                connection.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public List retrive(cEntityDomain entity){
        PreparedStatement pst = null;

        cBannerCard objBannerCard = (cBannerCard) entity;
        String sql=null;

        if (objBannerCard.getBanner() == null){
            objBannerCard.setBanner("");
        }

        if (objBannerCard.getId() == null && objBannerCard.getBanner().equals("")){
            sql = "SELECT * FROM " + acTableModels.BannerCards[0];
        }else if(objBannerCard.getId() != null && objBannerCard.getBanner().equals("")){
            sql = "SELECT * FROM " + acTableModels.BannerCards[0] + " WHERE " + acTableModels.BannerCards[1] + " = ?";
        }else if(objBannerCard.getId() == null && !objBannerCard.getBanner().equals("")){
            sql = "SELECT * FROM " + acTableModels.BannerCards[0] + " WHERE banner like ?";
        }

        try{
            openConnection();
            pst = connection.prepareStatement(sql);

            if (objBannerCard.getId() != null && objBannerCard.getBanner().equals("")){
                pst.setInt(1, objBannerCard.getId());
            }else if(objBannerCard.getId() == null && !objBannerCard.getBanner().equals("")){
                pst.setString(1, "%"+objBannerCard.getBanner()+"%");
            }

            ResultSet rs = pst.executeQuery();
            List<cEntityDomain> objBannerCards = new ArrayList<cEntityDomain>();

            while(rs.next()){
                cBannerCard objBannerCard2 = new cBannerCard();
                objBannerCard2.setId(rs.getInt(acTableModels.BannerCards[1]));
                objBannerCard2.setBanner(rs.getString("banner"));
                objBannerCard2.setActive(rs.getBoolean("active"));

                objBannerCards.add(objBannerCard2);
            }
            return objBannerCards;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
}

<%--
  Created by IntelliJ IDEA.
  User: geovane95
  Date: 01/10/17
  Time: 18:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>My Library</title>
    <meta charset="utf8"/>
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css"/>
    <style type="text/css">
        .jumbotron, .navbar{
            margin-top: 0px;
            margin-bottom: 0px;
        }
    </style>
</head>
<body>
<header class="jumbotron">
    <h1>My Library</h1>
</header>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#MyNavbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.jsp">My Library</a>
        </div>

        <div class="collapse navbar-collapse" id="MyNavbar">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Livros <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#createbook" data-toggle="tab">Inserir Livro</a></li>
                        <li><a href="#dropbook" data-toggle="tab">Deletar Livro</a></li>
                        <li><a href="#listbooks" data-toggle="tab">Listar Livros</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Categorias <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#createcategory" data-toggle="tab">Inserir Categorias</a></li>
                        <li><a href="#dropcategory" data-toggle="tab">Deletar Categorias</a></li>
                        <li><a href="#listcategorys" data-toggle="tab">Listar Categorias</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Usuarios <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#createuser" data-toggle="tab">Inserir Usuario</a></li>
                        <li><a href="#dropuser" data-toggle="tab">Deletar Usuario</a></li>
                        <li><a href="#listusers" data-toggle="tab">Listar Usuarios</a></li>
                    </ul>
                </li>
                <li class="dropdown active">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Bandeiras de Cartões<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Inserir Bandeira de Cartão</a></li>
                        <li><a href="listbannercard.jsp">Listar Bandeira de Cartão</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Autores <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#createauthor" data-toggle="tab">Inserir Autor</a></li>
                        <li><a href="#dropauthor" data-toggle="tab">Deletar Autor</a></li>
                        <li><a href="#listauthors" data-toggle="tab">Listar Autores</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Editoras <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#createeditor" data-toggle="tab">Inserir Editora</a></li>
                        <li><a href="#dropeditor" data-toggle="tab">Deletar Editora</a></li>
                        <li><a href="#listeditors" data-toggle="tab">Listar Editoras</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Grupo de Precificações <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#createpricinggroup" data-toggle="tab">Inserir Grupo de Precificação</a></li>
                        <li><a href="#droppricinggroup" data-toggle="tab">Deletar Grupo de Precificação</a></li>
                        <li><a href="#listpricinggroups" data-toggle="tab">Listar Grupo de Precificações</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<section>
    <article id="container">
        <div class="row text-center">
            <div class="col-md-3"></div>
            <div class="col-md-5 row">
                <fieldset>
                    <legend>Bandeiras de Cartões</legend>
                    <form class="form-horizontal col-md-12 row" action="Servlet" method="post">
                        <div class="form-group">
                            <label class="control-label col-md-12" for="txtBanner">Bandeira</label>
                            <input type="text" class="col-md-12" name="txtBanner" id="txtBanner"/>
                        </div>
                        <div class="form-group">
                            <input type="submit" id="operation" name="operation" value="create"/>
                            <input type="submit" id="operation" name="operation" value="drop"/>
                        </div>
                    </form>
                </fieldset>
            </div>
            <div class="col-md-4"></div>
        </div>
    </article>
</section>
<footer class="row">
</footer>
<script src="./js/jQuery.js"></script>
<script src="./js/bootstrap.min.js"></script>
</body>
</html>

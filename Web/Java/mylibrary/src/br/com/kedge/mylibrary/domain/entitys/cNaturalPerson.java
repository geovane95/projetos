package br.com.kedge.mylibrary.domain.entitys;

import java.util.Date;

public class cNaturalPerson extends acCustomer {
    public cNaturalPerson(){}
    public cNaturalPerson(int id, boolean active, String name, Date dtBeginLife, String email,
                          String password, Date dtReg, cPhone[] objPhones, String cpf, String rg){
        super(id, active, name, dtBeginLife, email, password, dtReg, objPhones);

        this.setCpf(cpf);
        this.setRg(rg);
    }

    private String cpf;

    private String rg;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }
}

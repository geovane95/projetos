package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cBooks extends cEntityDomain {

    public cBooks(){}

	public cBooks(int id, boolean active, String title, int year, int numpages, String sinopsis,
				  int barcode, double value, cISBN objISBN,
				  cDimensions objDimensions, cPricingGroup objPricingGroup,
				  cStockParameter objStockParameter, cSaleParameter objSaleParameter,
				  cEdition objEdition, cAuthor objAuthor, cEditors objEditors,
				  cCategoryUpper objCategoryUpper){
		super(id, active);
		this.setTitulo(title);
		this.setAno(year);
		this.setNumpaginas(numpages);
		this.setSinopse(sinopsis);
		this.setCodbarras(barcode);
		this.setValor(value);
		this.setObjISBN(objISBN);
		this.setObjDimensions(objDimensions);
		this.setObjPricingGroup(objPricingGroup);
		this.setObjStockParameter(objStockParameter);
		this.setObjSaleParameter(objSaleParameter);
		this.setObjEdition(objEdition);
		this.setObjAuthor(objAuthor);
		this.setObjEditors(objEditors);
		this.setObjCategoryUpper(objCategoryUpper);
	}

	private String titulo;

	private int ano;

	private int numpaginas;

	private String sinopse;

	private int codbarras;

	private double valor;

	private cISBN objISBN;

	private cDimensions objDimensions;

	private cPricingGroup objPricingGroup;

	private cStockParameter objStockParameter;

	private cSaleParameter objSaleParameter;

	private cEdition objEdition;

	private cAuthor objAuthor;

	private cEditors objEditors;

	private cCategoryUpper objCategoryUpper;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getNumpaginas() {
		return numpaginas;
	}

	public void setNumpaginas(int numpaginas) {
		this.numpaginas = numpaginas;
	}

	public String getSinopse() {
		return sinopse;
	}

	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}

	public int getCodbarras() {
		return codbarras;
	}

	public void setCodbarras(int codbarras) {
		this.codbarras = codbarras;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public cISBN getObjISBN() {
		return objISBN;
	}

	public void setObjISBN(cISBN objISBN) {
		this.objISBN = objISBN;
	}

	public cDimensions getObjDimensions() {
		return objDimensions;
	}

	public void setObjDimensions(cDimensions objDimensions) {
		this.objDimensions = objDimensions;
	}

	public cPricingGroup getObjPricingGroup() {
		return objPricingGroup;
	}

	public void setObjPricingGroup(cPricingGroup objPricingGroup) {
		this.objPricingGroup = objPricingGroup;
	}

	public cStockParameter getObjStockParameter() {
		return objStockParameter;
	}

	public void setObjStockParameter(cStockParameter objStockParameter) {
		this.objStockParameter = objStockParameter;
	}

	public cSaleParameter getObjSaleParameter() {
		return objSaleParameter;
	}

	public void setObjSaleParameter(cSaleParameter objSaleParameter) {
		this.objSaleParameter = objSaleParameter;
	}

	public cEdition getObjEdition() {
		return objEdition;
	}

	public void setObjEdition(cEdition objEdition) {
		this.objEdition = objEdition;
	}

	public cAuthor getObjAuthor() {
		return objAuthor;
	}

	public void setObjAuthor(cAuthor objAuthor) {
		this.objAuthor = objAuthor;
	}

	public cEditors getObjEditors() {
		return objEditors;
	}

	public void setObjEditors(cEditors objEditors) {
		this.objEditors = objEditors;
	}

	public cCategoryUpper getObjCategoryUpper() {
		return objCategoryUpper;
	}

	public void setObjCategoryUpper(cCategoryUpper objCategoryUpper) {
		this.objCategoryUpper = objCategoryUpper;
	}
}

package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cAddress extends cEntityDomain {
    public cAddress(){}

    public cAddress(Integer id, boolean active, cTypeHouse objTypeHouse,
                    cStreet objStreet, String number, String neighborhood,
                    cCity objCity, String zipcode, boolean preferential,
                    String comments, int user, cTypeAddress objTypeAddress){
        super(id, active);

        this.setObjTypeHouse(objTypeHouse);
        this.setObjStreet(objStreet);
        this.setNumber(number);
        this.setNeighborhood(neighborhood);
        this.setObjCity(objCity);
        this.setZipcode(zipcode);
        this.setPreferential(preferential);
        this.setComments(comments);
        this.setUser(user);
        this.setObjTypeAddress(objTypeAddress);
    }

    private cTypeHouse objTypeHouse;

    private cStreet objStreet;

    private String number;

    private String neighborhood;

    private cCity objCity;

    private String zipcode;

    private boolean preferential;

    private String comments;

    private int user;

    private cTypeAddress objTypeAddress;



    public cTypeHouse getObjTypeHouse() {
        return objTypeHouse;
    }

    public void setObjTypeHouse(cTypeHouse objTypeHouse) {
        this.objTypeHouse = objTypeHouse;
    }

    public cStreet getObjStreet() {
        return objStreet;
    }

    public void setObjStreet(cStreet objStreet) {
        this.objStreet = objStreet;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public cCity getObjCity() {
        return objCity;
    }

    public void setObjCity(cCity objCity) {
        this.objCity = objCity;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public boolean isPreferential() {
        return preferential;
    }

    public void setPreferential(boolean preferential) {
        this.preferential = preferential;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public cTypeAddress getObjTypeAddress() {
        return objTypeAddress;
    }

    public void setObjTypeAddress(cTypeAddress objTypeAddress) {
        this.objTypeAddress = objTypeAddress;
    }
}

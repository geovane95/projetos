package br.com.kedge.mylibrary.web.viewhelper.implementation;

import br.com.kedge.mylibrary.core.aplication.cResult;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entitys.cBannerCard;
import br.com.kedge.mylibrary.web.viewhelper.iViewHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class cBannerCardViewHelper implements iViewHelper {
    @Override
    public cEntityDomain getEntity(HttpServletRequest request) {

        String operation = request.getParameter("operation");
        cBannerCard objBannerCard = null;

        if (!operation.equals("view")){
            String banner = request.getParameter("txtBanner");
            String id = request.getParameter("txtId");
            String active = request.getParameter("optActive");

            objBannerCard = new cBannerCard();

            if (banner != null && !banner.trim().equals("")){
                objBannerCard.setBanner(banner);
            }
            if (id != null && !id.trim().equals(""));{
                objBannerCard.setId(Integer.parseInt(id));
            }
            if (active != null && !active.trim().equals("")){
                if (active != "inactive"){
                    objBannerCard.setActive(true);
                }else{
                    objBannerCard.setActive(false);
                }
            }
        }else{
            HttpSession session = request.getSession();
            cResult objResult = (cResult) session.getAttribute("objResult");
            String sId = request.getParameter("txtId");
            int iId = 0;

            if (sId != null && !sId.trim().equals("")){
                iId = Integer.parseInt(sId);
            }

            for (cEntityDomain entity: objResult.getEntitys()){
                if (entity.getId() == iId){
                    objBannerCard = (cBannerCard) entity;
                }
            }
        }
        return objBannerCard;
    }


    public void setView(cResult objResult, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        RequestDispatcher rd = null;

        String operation = request.getParameter("operation");

        if (objResult.getMsg() == null){
            if (operation.equals("create")){
                objResult.setMsg("Bandeira de Cartão cadastrada com sucesso!");
            }

            request.getSession().setAttribute("objRseult", objResult);
            rd = request.getRequestDispatcher("FormRetriveBannerCard.jsp");
        }

        if (objResult.getMsg() == null && operation.equals("update")){
            rd = request.getRequestDispatcher("FormRetriveBannerCard.jsp");
        }

        if (objResult.getMsg() == null && operation.equals("view")){
            request.setAttribute("objBannerCard", objResult.getEntitys().get(0));
            rd = request.getRequestDispatcher("FormBannerCard.jsp");
        }

        if (objResult.getMsg() == null && operation.equals("drop")){
            request.getSession().setAttribute("objResult", null);
            rd = request.getRequestDispatcher("FormRetriveBannerCard.jsp");
        }

        if (objResult.getMsg() != null){
            if (operation.equals("create") || operation.equals("update")){
                request.getSession().setAttribute("objResult", objResult);
                rd = request.getRequestDispatcher("FormRetriveBannerCard.jsp");
            }
        }

        rd.forward(request,response);
    }
}

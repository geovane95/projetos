package br.com.kedge.mylibrary.domain.entitys;

import java.util.Date;

public class cLegalPerson extends acCustomer {
    public cLegalPerson(){}

    public cLegalPerson(int id, boolean active, String namingTrade, String email, Date dtBeginLife, String password,
                        Date dtReg, cPhone[] objPhones, String cnpj, String companyName,
                        String stateReg, String municipalReg){
        super(id, active, namingTrade, dtBeginLife, email, password, dtReg, objPhones);
        this.setCnpj(cnpj);

        this.setRazaoSocial(companyName);

        this.setRegistroMunicipal(municipalReg);

        this.setRegistroEstadual(stateReg);
    }

    private String cnpj;

    private String razaoSocial;

    private String registroEstadual;

    private String registroMunicipal;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getRegistroEstadual() {
        return registroEstadual;
    }

    public void setRegistroEstadual(String registroEstadual) {
        this.registroEstadual = registroEstadual;
    }

    public String getRegistroMunicipal() {
        return registroMunicipal;
    }

    public void setRegistroMunicipal(String registroMunicipal) {
        this.registroMunicipal = registroMunicipal;
    }
}

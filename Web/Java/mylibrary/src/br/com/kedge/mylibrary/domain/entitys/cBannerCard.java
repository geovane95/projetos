package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cBannerCard extends cEntityDomain {

    public cBannerCard(){}

    public cBannerCard(int id, boolean active, String banner){
        super(id, active);

        this.setBanner(banner);
    }

    private String banner;

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}

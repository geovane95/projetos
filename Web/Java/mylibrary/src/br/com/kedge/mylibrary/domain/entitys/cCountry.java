package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cCountry extends cEntityDomain {

    public cCountry(){}

    public cCountry(Integer id, boolean active, String country){
        super(id, active);

        this.setCountry(country);
    }

    private String country;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}

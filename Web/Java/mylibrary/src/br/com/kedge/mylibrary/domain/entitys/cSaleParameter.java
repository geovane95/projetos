package br.com.kedge.mylibrary.domain.entitys;

public class cSaleParameter {

	public cSaleParameter(int tempo_recorrencia, int qtd_minima_livros){
		this.setTempo_recorrencia(tempo_recorrencia);
		this.setQtd_minima_livros(qtd_minima_livros);

	}

	private int tempo_recorrencia;

	private int qtd_minima_livros;

	public int getTempo_recorrencia() {
		return tempo_recorrencia;
	}

	public void setTempo_recorrencia(int tempo_recorrencia) {
		this.tempo_recorrencia = tempo_recorrencia;
	}

	public int getQtd_minima_livros() {
		return qtd_minima_livros;
	}

	public void setQtd_minima_livros(int qtd_minima_livros) {
		this.qtd_minima_livros = qtd_minima_livros;
	}
}

package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

import java.util.Date;

public abstract class acPerson extends cEntityDomain {
    public acPerson(){}
    public acPerson(int id, boolean active, String name, Date dtBeginLife){
        super(id, active);

        this.setName(name);
        this.setDtBeginLife(dtBeginLife);
    }


    private String name;

    private Date dtBeginLife;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDtBeginLife() {
        return dtBeginLife;
    }

    public void setDtBeginLife(Date dtBeginLife) {
        this.dtBeginLife = dtBeginLife;
    }
}

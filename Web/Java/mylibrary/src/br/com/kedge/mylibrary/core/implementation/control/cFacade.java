package br.com.kedge.mylibrary.core.implementation.control;

import br.com.kedge.mylibrary.core.iDao;
import br.com.kedge.mylibrary.core.iFacade;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import br.com.kedge.mylibrary.core.aplication.cResult;
import br.com.kedge.mylibrary.core.iStrategy;
import br.com.kedge.mylibrary.core.implementation.dao.cBooksDao;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entitys.cBooks;

public class cFacade implements iFacade {

	private Map<String, iDao> daos;

	private Map<String, Map<String, List<iStrategy>>> rns;

	private cResult objResult;

	public cFacade() {
		daos = new HashMap<String, iDao>();
		rns = new HashMap<String, Map<String, List<iStrategy>>>();

		cBooksDao objBooksDao = new cBooksDao();

		daos.put(cBooks.class.getName(), objBooksDao);

		/*Criar instancias dasregras de negócio*/

		/*Criar lista de Strategys*/

		/*Adicionar objs criados das strategys a lista*/

		/* Cria o mapa que poderá conter todas as listas de regras de negócio específica
		 * por operação  do fornecedor
		 */

		/*
		 * Adiciona a listra de regras na operação salvar no mapa do fornecedor (lista criada na linha 70)
		 */


		/* Adiciona o mapa(criado na linha 79) com as regras indexadas pelas operações no mapa geral indexado
		 * pelo nome da entidade
		 */
	}

    @Override
	public cResult create(cEntityDomain entity) {
		objResult = new cResult();
			String nmClass = entity.getClass().getName();

			String msg = executeRules(entity, "Create");

			if (msg == null){
			    iDao objIDao = daos.get(nmClass);
			    try{
			        objIDao.create(entity);
			        List<cEntityDomain> entitys = new ArrayList<>();
			        entitys.add(entity);
			        objResult.setEntitys(entitys);
                }catch (SQLException e){
			        e.printStackTrace();
			        objResult.setMsg("Não foi possivel realizar o registro!");
                }
            }else objResult.setMsg(msg);

            return objResult;
	}

	public cResult update(cEntityDomain entity) {
	    objResult = new cResult();

	    String nmClass = entity.getClass().getName();

	    String msg = executeRules(entity,"Update");

	    if (msg == null){
	        iDao objIDao = daos.get(nmClass);
	        try{
	            objIDao.update(entity);
	            List<cEntityDomain> entitys = new ArrayList<cEntityDomain>();

	            entitys.add(entity);
	            objResult.setEntitys(entitys);
            }catch (SQLException e){
	            e.printStackTrace();
	            objResult.setMsg("Não foi possivel realizar a alteração");
            }
        }else{
	        objResult.setMsg(msg);
        }
		return objResult;
	}

	@Override
	public cResult drop(cEntityDomain entity) {
	    objResult = new cResult();

	    String nmClass = entity.getClass().getName();

	    String msg = executeRules(entity, "Drop");

	    if (msg == null){
	        iDao objIDao = daos.get(nmClass);
	        try{
	            objIDao.drop(entity);
                List<cEntityDomain> entitys = new ArrayList<cEntityDomain>();
                entitys.add(entity);
                objResult.setMsg("Não foi possivel excluir o registro");
            }catch(SQLException e){
	            e.printStackTrace();
	            objResult.setMsg(msg);
            }
        }

		return objResult;
	}

	@Override
	public cResult retrive(cEntityDomain entity) {
	    objResult = new cResult();

	    String nmClasse = entity.getClass().getName();

	    String msg = executeRules(entity, "Retrive");

	    if (msg == null){
	        iDao objIDao = daos.get(nmClasse);
	        try{
	            objResult.setEntitys(objIDao.retrive(entity));
            }catch (SQLException e){
	            e.printStackTrace();
	            objResult.setMsg("Não foi possível realizar o registro!");
            }
        }else{
	        objResult.setMsg(msg);
        }
		return objResult;
	}

	@Override
	public cResult view(cEntityDomain entity) {
	    objResult = new cResult();
	    objResult.setEntitys(new ArrayList<cEntityDomain>(1));
	    objResult.getEntitys().add(entity);
		return objResult;
	}

	public String executeRules(cEntityDomain entity, String operation) {

	    String nmClass = entity.getClass().getName();

	    StringBuilder msg = new StringBuilder();

	    Map<String, List<iStrategy>> rulesOperation = rns.get(nmClass);

	    if (rulesOperation != null){
	        List<iStrategy> rules = rulesOperation.get(operation);
            for (iStrategy s: rules){
                String m = s.process(entity);

                if (m != null){
                    msg.append(m);
                    msg.append("\n");
                }
            }
        }

        if (msg.length()>0){
	        return msg.toString();
        }else{
            return null;
        }
	}

}

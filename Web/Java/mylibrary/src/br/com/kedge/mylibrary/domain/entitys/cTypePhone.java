package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cTypePhone extends cEntityDomain {

    public cTypePhone(int id, boolean active, String type){
        super(id, active);

        this.setType(type);
    }

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

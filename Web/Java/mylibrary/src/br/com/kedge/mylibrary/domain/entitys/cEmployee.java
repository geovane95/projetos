package br.com.kedge.mylibrary.domain.entitys;

import java.util.Date;

public class cEmployee extends cNaturalPerson{

    public cEmployee(){}

    public cEmployee(int id, boolean active, String name, Date dtBeginLife, String email, String password,
                     Date dtReg, cPhone[] objPhones, String cpf, String rg, String workPermit){
        super(id, active, name, dtBeginLife, email, password, dtReg, objPhones, cpf, rg);


        this.setWorkPermit(workPermit);
    }

    private String workPermit;

    public String getWorkPermit() {
        return workPermit;
    }

    public void setWorkPermit(String workPermit) {
        this.workPermit = workPermit;
    }
}

package br.com.kedge.mylibrary.domain.entitys;

import java.util.Date;

public abstract class acCustomer extends acUser {

    public acCustomer(){}

    public acCustomer(Integer id, boolean active, String name, Date dtBeginLife, String email,
                      String password, Date dtReg, cPhone[] objPhones){
        super(id, active, name, dtBeginLife, email, password, dtReg);

        this.setObjPhones(objPhones);
    }

    private cPhone[] objPhones;

    public cPhone[] getObjPhones() {
        return objPhones;
    }

    public void setObjPhones(cPhone[] objPhones) {
        this.objPhones = objPhones;
    }
}

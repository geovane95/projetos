package br.com.kedge.mylibrary.core.implementation.dao;

import br.com.kedge.mylibrary.core.util.acTableModels;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entitys.cNaturalPerson;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class cNaturalPersonsDao extends acJdbcDao{

    protected cNaturalPersonsDao() {
        super(acTableModels.NaturalPersons[0], acTableModels.NaturalPersons[1]);
    }

    @Override
    public void create(cEntityDomain entity){
        openConnection();
        PreparedStatement pst = null;
        cNaturalPerson objNaturalPerson = (cNaturalPerson) entity;

        try{
            connection.setAutoCommit(false);

            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO naturalperson (firstname, middlename, lastname, email, cpf, rg, dtbirth)");
            sql.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

            pst = connection.prepareStatement(sql.toString());

            pst.executeUpdate();
            connection.commit();
        }catch(SQLException e){
            try {
                connection.rollback();
            }catch(SQLException se){
                se.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void update(cEntityDomain entity){

    }

    @Override
    public List retrive(cEntityDomain entity){
        return null;
    }
}

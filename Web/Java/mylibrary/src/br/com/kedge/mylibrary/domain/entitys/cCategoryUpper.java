package br.com.kedge.mylibrary.domain.entitys;

public class cCategoryUpper extends acCategories {

    public cCategoryUpper(int id, boolean active, String name, cCategoryLower objCategoriesLowers){
        super(id, active, name);
        this.objCategoriesLowers = objCategoriesLowers;
    }
    private cCategoryLower objCategoriesLowers;

    public cCategoryLower getObjCategoriesLowers() {
        return objCategoriesLowers;
    }

    public void setObjCategories(cCategoryLower objCategoriesLowers) {
        this.objCategoriesLowers = objCategoriesLowers;
    }
}

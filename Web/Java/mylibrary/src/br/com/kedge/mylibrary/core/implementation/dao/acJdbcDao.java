package br.com.kedge.mylibrary.core.implementation.dao;

import br.com.kedge.mylibrary.core.iDao;
import br.com.kedge.mylibrary.core.util.cConection;
import br.com.kedge.mylibrary.domain.cEntityDomain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class acJdbcDao implements iDao{

	protected Connection connection;

	protected String table;

	protected String idTable;

	protected boolean ctrlTransaction;

	public acJdbcDao(Connection connection, String table, String idTable) {

	}

	protected acJdbcDao(String table, String idTable) {

	}

	public void drop(cEntityDomain entity) {
		openConnection();
        PreparedStatement pst = null;
        StringBuilder sb = new StringBuilder();
        sb.append("ALTER TABLE");
        sb.append(table);
        sb.append (" SET active=false WHERE ");
        sb.append(idTable);
        sb.append(" = ");
        sb.append("?");

        try {
            connection.setAutoCommit(false);
            pst = connection.prepareStatement(sb.toString());
            pst.setInt(1,entity.getId());

            pst.executeUpdate();
            connection.commit();
        }catch(SQLException e){
            try{
                connection.rollback();
            }catch (SQLException ex){
                ex.printStackTrace();
            }
            e.printStackTrace();
        }finally {
            try{
                pst.close();
                if (ctrlTransaction){
                    connection.close();
                }
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
	}

	protected void openConnection() {
		try{
            if (connection == null || connection.isClosed()){
                connection = cConection.getConnection();
            }
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        }catch (SQLException e){
            e.printStackTrace();
        }
	}

}

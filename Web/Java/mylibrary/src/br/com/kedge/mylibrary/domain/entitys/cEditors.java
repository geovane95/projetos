package br.com.kedge.mylibrary.domain.entitys;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cEditors extends cEntityDomain {

	public cEditors(int id, boolean active, String nome){
		super(id, active);
		this.nome = nome;
	}

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}

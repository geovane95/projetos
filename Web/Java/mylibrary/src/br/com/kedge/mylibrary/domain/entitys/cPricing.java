package br.com.kedge.mylibrary.domain.entitys;

public class cPricing {

	public cPricing(double margem_lucro){
		this.margem_lucro = margem_lucro;
	}
	private double margem_lucro;

	public double getMargem_lucro() {
		return margem_lucro;
	}

	public void setMargem_lucro(double margem_lucro) {
		this.margem_lucro = margem_lucro;
	}
}

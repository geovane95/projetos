<%@ page import="br.com.kedge.mylibrary.domain.entities.cBooks" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: geovane95
  Date: 14/10/17
  Time: 08:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Livro</title>
</head>
<body>
<% cBooks objBook = (cBooks) request.getAttribute("objbook"); %>
<header>

</header>
<nav>

</nav>
<section>
    <article>
        <form action="Book" method="post">
            <fieldset>
                <legend>Livro</legend>
                <div>
                    <label for="txtId">Id: </label>
                    <input type='text' name='txtId' id='txtId' placeholder='0'
                            <%
                                if (objBook != null) {
                                    out.print("value='" + objBook.getId() + "' readonly");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtTitle">Nome: </label>
                    <input type='text' name='txtTitle' id='txtTitle' placeholder='Titulo do Livro'
                            <%
                                if (objBook != null) {
                                    out.print("value='" + objBook.getTitle() + "'");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtSynopsis">Sinopse: </label>
                    <input type='text' name='txtSynopsis' id='txtSynopsis' placeholder='Sobre o que o livro fala?'
                            <%
                                if (objBook != null) {
                                    out.print("value='" + objBook.getSynopsis() + "'");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtdtRegister">Data de Registro: </label>
                    <input type='text' name='txtdtRegister' id='txtdtRegister' placeholder='10'
                            <%
                                if (objBook != null) {
                                    out.print("value='" + objBook.getDtRegister() + "'");
                                } else {
                                    try {
                                        Date today = new Date(System.currentTimeMillis());
                                        DateFormat dtToday = new SimpleDateFormat("dd/MM/yyyy");
                                        today = dtToday.parse(today.toString());
                                        out.print("value='" + today + "'");
                                    } catch (ParseException pe) {
                                        pe.printStackTrace();
                                    }
                                }
                            %>
                           readonly/>
                </div>
                <div>
                    <label for="txtActive1"><label for="txtActive2">Ativo:</label></label>
                    <label>
                        <input type='radio' name='txtActive' id='txtActive1' value='true' checked/>Sim
                    </label>
                    <label>
                        <input type='radio' name='txtActive' id='txtActive2' value='false'
                                <%
                                    if (objBook != null && !objBook.isActive()) {
                                        out.print("checked");
                                    }
                                %>
                        />Não
                    </label>
                </div>
                <div>
                    <%
                        if (objBook != null) {
                            out.print("<input type='submit' class='btn btn-primary' name='operation' id='operation' value='Alterar'/>");
                            out.print("<input type='submit' class='btn btn-danger' name='operation' id='operation' value='Inativar'/>");
                        } else {
                            out.print("<input type='submit' class='btn btn-success' name='operation' id='operation' value='Salvar'/>");
                        }

                    %>
                </div>
            </fieldset>
        </form>
    </article>
</section>
<footer>

</footer>
</body>
</html>

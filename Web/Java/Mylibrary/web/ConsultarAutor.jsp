<%@ page import="br.com.kedge.mylibrary.core.aplication.cResult" %>
<%@ page import="br.com.kedge.mylibrary.domain.cEntityDomain" %>
<%@ page import="br.com.kedge.mylibrary.domain.entities.cAuthor" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: geovane95
  Date: 13/10/17
  Time: 19:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Consulta Autor</title>
</head>
<body>
<%
    cResult objResult = (cResult) session.getAttribute("objresult");
%>
<header>

</header>
<nav>

</nav>
<section>
    <article>
        <form action="Author" method="post">
            <fieldset>
                <legend>Author</legend>
                <div>
                    <label for="txtId">Id: </label>
                    <input type='text' name='txtId' id='txtId' placeholder='0'/>
                </div>
                <div>
                    <label for="txtName">Nome: </label>
                    <input type='text' name='txtName' id='txtName' placeholder='Seu nome'/>
                </div>
                <div>
                    <label for="txtdtBirth">Data: </label>
                    <input type='text' name='txtdtBirth' id='txtdtBirth' placeholder='29/02/1996'/>
                </div>
                <div>
                    <label for="txtBooks">Livros: </label>
                    <input type='number' name='txtBooks' id='txtBooks' placeholder='10'/>
                </div>
                <div>
                    <label for="txtActive1" for="txtActive2">Ativo: </label>
                    <label>
                        <input type='radio' name='txtActive' id='txtActive1' value='true' checked/>Sim
                    </label>
                    <label>
                        <input type='radio' name='txtActive' id='txtActive2' value='false'/>Não
                    </label>
                </div>
                <div>
                    <input type='submit' class='btn btn-success' name='operation' id='operation' value='Consultar'/>
                </div>
            </fieldset>
        </form>
        <%

            if (objResult != null && objResult.getMsg() != null) {
                out.print(objResult.getMsg());
            }

        %>
    </article>
    <article>
        <table>
            <fieldset>
                <legend>Autores</legend>
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>Nome</th>
                    <th>Nascimento</th>
                    <th>Livros</th>
                    <th>Ativo</th>
                    <th>Alterar</th>
                    <th>Ativar/Inativar</th>
                    <th>Visualizar</th>
                </tr>
                </thead>
                <tbody>
                <%
                    if (objResult != null) {
                        List<cEntityDomain> objEntities = objResult.getEntities();
                        StringBuilder criaTabela = new StringBuilder();
                        if (objEntities != null) {
                            for (cEntityDomain objED : objEntities) {
                                cAuthor objAu = (cAuthor) objED;
                                criaTabela.append("<tr>");
                                criaTabela.append("<td>");
                                criaTabela.append(objAu.getId());
                                criaTabela.append("</td>");
                                criaTabela.append("<td>");
                                criaTabela.append(objAu.getName());
                                criaTabela.append("</td>");
                                criaTabela.append("<td>");
                                criaTabela.append(objAu.getDtBeginLife());
                                criaTabela.append("</td>");
                                criaTabela.append("<td>");
                                criaTabela.append(objAu.getBooks());
                                criaTabela.append("</td>");
                                criaTabela.append("<td>");
                                if (objAu.isActive()) {
                                    criaTabela.append("Sim");
                                } else {
                                    criaTabela.append("Não");
                                }
                                criaTabela.append("</td>");
                                criaTabela.append("<td>");
                                criaTabela.append("<a href='Author?txtId=" + objAu.getId() + "&operation=Alterar'><button type='button' class='btn btn-primary'>Alterar</button></a>");
                                criaTabela.append("</td>");
                                criaTabela.append("<td>");
                                if (objAu.isActive()) {
                                    criaTabela.append("<a href='Author?txtId=" + objAu.getId() + "&operation=Inativar'><button type='button' class='btn btn-danger'>Inativar</button></a>");
                                } else {
                                    criaTabela.append("<a href='Author?txtId=" + objAu.getId() + "&operation=Ativar'><button type='button' class='btn btn-warning'>Ativar</button></a>");
                                }
                                criaTabela.append("</td>");
                                criaTabela.append("<td>");
                                criaTabela.append("<a href='Author?txtId=" + objAu.getId() + "&operation=Visualizar'><button type='button' class='btn btn-success'>Visualizar</button></a>");
                                criaTabela.append("</td>");
                                criaTabela.append("</tr>");


                                out.print(criaTabela.toString());
                            }
                        }
                    } else {
                        out.print("<tr><td colspan='8'>Nenhum dado a ser mostrado!</td></tr>");
                    }
                %>
                </tbody>
            </fieldset>
        </table>
    </article>
</section>
<footer>

</footer>

</body>
</html>

<%@ page import="br.com.kedge.mylibrary.domain.entities.cAuthor" %><%--
  Created by IntelliJ IDEA.
  User: geovane95
  Date: 13/10/17
  Time: 19:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Autor</title>
</head>
<body>
<% cAuthor objAuthor = (cAuthor) request.getAttribute("objauthor"); %>
<header>

</header>
<nav>

</nav>
<section>
    <article>
        <form action="Author" method="get">
            <fieldset>
                <legend>Autor</legend>
                <div>
                    <label for="txtId">Id: </label>
                    <input type='text' name='txtId' id='txtId' placeholder='0'
                            <%
                                if (objAuthor != null) {
                                    out.print("value='" + objAuthor.getId() + "' readonly");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtName">Nome: </label>
                    <input type='text' name='txtName' id='txtName' placeholder='Seu nome'
                            <%
                                if (objAuthor != null) {
                                    out.print("value='" + objAuthor.getName() + "'");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtdtBirth">Data: </label>
                    <input type='text' name='txtdtBirth' id='txtdtBirth' placeholder='29/02/1996'
                            <%
                                if (objAuthor != null) {
                                    out.print("value='" + objAuthor.getDtBeginLife() + "'");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtBooks">Livros Cadastrados: </label>
                    <input type='text' name='txtBooks' id='txtBooks' placeholder='10'
                            <%
                                if (objAuthor != null) {
                                    out.print("value='" + objAuthor.getBooks() + "'");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtActive">Ativo: </label>
                    <label>
                        <input type='radio' name='txtActive' id='txtActive' value='true' checked='true'/>Sim
                    </label>
                    <label>
                        <input type='radio' name='txtActive' id='txtActive' value='false'
                                <%
                                    if (objAuthor != null && !objAuthor.isActive()) {
                                        out.print("checked='true'");
                                    }
                                %>
                        />Não
                    </label>
                </div>
                <div>
                    <%
                        if (objAuthor != null) {
                            out.print("<input type='submit' class='btn btn-primary' name='operation' id='operation' value='Alterar'/>");
                            out.print("<input type='submit' class='btn btn-danger' name='operation' id='operation' value='Inativar'/>");
                        } else {
                            out.print("<input type='submit' class='btn btn-success' name='operation' id='operation' value='Salvar'/>");
                        }

                    %>
                </div>
            </fieldset>
        </form>
    </article>
</section>
<footer>

</footer>
</body>
</html>

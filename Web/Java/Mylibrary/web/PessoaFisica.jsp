<%--
  Created by IntelliJ IDEA.
  User: geovane95
  Date: 09/10/17
  Time: 16:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page
        import="br.com.kedge.mylibrary.core.implementation.dao.cAccessLevelDao, br.com.kedge.mylibrary.domain.cEntityDomain, br.com.kedge.mylibrary.domain.entities.cAccessLevel" %>
<%@ page import="br.com.kedge.mylibrary.domain.entities.cNaturalPerson" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.List" %>
<html>
<head>
    <title>Pessoa Fisica</title>
</head>
<body>
<%
    cEntityDomain objEntityDomain = new cEntityDomain();
    cAccessLevel objAccessLevel = new cAccessLevel();
    cAccessLevelDao objAccessLevelDao = new cAccessLevelDao();
    cNaturalPerson objNaturalPerson = (cNaturalPerson) request.getAttribute("objNaturalPerson");
    List<cAccessLevel> objAccessLevels = new ArrayList<cAccessLevel>((Collection<? extends cAccessLevel>) objAccessLevelDao.retrive(objEntityDomain));
%>
<header>

</header>
<nav>

</nav>
<section>
    <article>
        <form action="AccessLevel" method="post">
            <fieldset>
                <legend>Nivel De Acesso</legend>
                <div>
                    <label for="txtId">Id: </label>
                    <input type='text' name='txtId' id='txtId' placeholder='0'
                            <%
                                if (objNaturalPerson != null) {
                                    out.print("value='" + objNaturalPerson.getId() + "' readonly>");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtName">Nome: </label>
                    <input type='text' name='txtName' id='txtName' placeholder='Seu nome'
                            <%
                                if (objNaturalPerson != null) {
                                    out.print("value='" + objNaturalPerson.getName() + "'>");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtCpf">Cpf: </label>
                    <input type='number' name='txtCpf' id='txtCpf' placeholder='00000000000'
                            <%
                                if (objNaturalPerson != null) {
                                    out.print("value='" + objNaturalPerson.getCpf() + "'>");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtRg">Rg: </label>
                    <input type='text' name='txtRg' id='txtRg' placeholder='000000000'
                            <%
                                if (objNaturalPerson != null) {
                                    out.print("value='" + objNaturalPerson.getGeneralRegister() + "'>");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtEmail">Email: </label>
                    <input type='email' name='txtEmail' id='txtEmail' placeholder='name@mymail.com'
                            <%
                                if (objNaturalPerson != null) {
                                    out.print("value='" + objNaturalPerson.getEmail() + "'>");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtPassword">Senha: </label>
                    <input type='password' name='txtPassword' id='txtPassword' placeholder='********'
                            <%
                                if (objNaturalPerson != null) {
                                    out.print("value='" + objNaturalPerson.getPassword() + "'>");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtDtBirth">Data de Nascimento: </label>
                    <input type='date' name='txtDtBirth' id='txtDtBirth' placeholder='11/10/2017'
                            <%
                                if (objNaturalPerson != null) {
                                    out.print("value='" + objNaturalPerson.getDtBeginLife() + "'>");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="optAccess">Acesso: </label>
                    <select name="optAccess" id="optAccess">
                        <%
                            if (objNaturalPerson != null) {
                                out.print("<option value='" + objNaturalPerson.getObjAccessLevel().getId() + "' >" + objNaturalPerson.getObjAccessLevel().getAccess() + "</option>");
                            } else {
                                out.print("<option value='0'></option>");
                            }
                        %>
                        <%
                            for (cAccessLevel objAL : objAccessLevels) {
                                out.print("<option value='" + objAL.getId() + "'>" + objAL.getAccess() + "</option>");
                            }
                        %>
                    </select>
                    />
                </div>
                <div>
                    <label for="txtRegister">Data de Registro: </label>
                    <input type='datetime-local' name='txtRegister' id='txtRegister' placeholder='11/10/2017'
                            <%
                                if (objNaturalPerson != null) {
                                    out.print("value='" + objNaturalPerson.getDtRegister() + "' readonly>");
                                }
                            %>
                    />
                </div>
                <div>
                    <label for="txtActive">Ativo: </label>
                    <label>
                        <input type='radio' name='txtActive' id='txtActive' value='true'
                                <%
                                    if (objNaturalPerson != null && objNaturalPerson.isActive()) {
                                        out.print("checked='true'");
                                    }
                                %>
                        />Sim
                    </label>
                    <label>
                        <input type='radio' name='txtActive' id='txtActive' value='false'
                                <%
                                    if (objNaturalPerson != null && !objNaturalPerson.isActive()) {
                                        out.print("checked='false'");
                                    }
                                %>
                        />Não
                    </label>
                </div>
                <div>
                    <%
                        if (objAccessLevel != null) {
                            out.print("<input type='submit' name='operation' id='operation' value='Alterar'/>");
                            out.print("<input type='submit' name='operation' id='operation' value='Excluir'/>");
                        } else {
                            out.print("<input type='submit' name='operation' id='operation' value='Salvar'/>");
                        }

                    %>
                </div>
            </fieldset>
        </form>
    </article>
</section>
<footer>

</footer>
</body>
</html>

<%@ page import="br.com.kedge.mylibrary.domain.entities.cAuthor" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: geovane95
  Date: 13/10/17
  Time: 19:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Lista Autores</title>
</head>
<body>
<header>

</header>
<nav>

</nav>
<section>
    <article>
        <table>
            <fieldset>
                <legend>Autores</legend>
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>Nome</th>
                    <th>Nascimento</th>
                    <th>Livros</th>
                    <th>Ativo</th>
                    <th>Alterar</th>
                    <th>Ativar/Inativar</th>
                    <th>Consultar</th>
                </tr>
                </thead>
                <tbody>
                <%
                    List<cAuthor> objauthors = (List<cAuthor>) request.getAttribute("objauthors");
                    if (objauthors != null) {
                        StringBuilder criaTabela = new StringBuilder();

                        for (cAuthor objAu : objauthors) {
                            criaTabela.append("<tr>");
                            criaTabela.append("<td>");
                            criaTabela.append(objAu.getId());
                            criaTabela.append("</td>");
                            criaTabela.append("<td>");
                            criaTabela.append(objAu.getName());
                            criaTabela.append("</td>");
                            criaTabela.append("<td>");
                            criaTabela.append(objAu.getDtBeginLife());
                            criaTabela.append("</td>");
                            criaTabela.append("<td>");
                            criaTabela.append(objAu.getBooks());
                            criaTabela.append("</td>");
                            criaTabela.append("<td>");
                            if (objAu.isActive()) {
                                criaTabela.append("Sim");
                            } else {
                                criaTabela.append("Não");
                            }
                            criaTabela.append("</td>");
                            criaTabela.append("<td>");
                            criaTabela.append("<a href='Author?id=" + objAu.getId() + "operation=Alterar'><button type='button' class='btn btn-primary'>Alterar</button></a>");
                            criaTabela.append("</td>");
                            criaTabela.append("<td>");
                            if (objAu.isActive()) {
                                criaTabela.append("<a href='Author?id=" + objAu.getId() + "operation=Inativar'><button type='button' class='btn btn-danger'>Inativar</button></a>");
                            } else {
                                criaTabela.append("<a href='Author?id=" + objAu.getId() + "operation=Ativar'><button type='button' class='btn btn-warning'>Ativar</button></a>");
                            }
                            criaTabela.append("</td>");
                            criaTabela.append("<td>");
                            criaTabela.append("<a href='Author?id=" + objAu.getId() + "operation=Consultar'><button type='button' class='btn btn-success'>Consultar</button></a>");
                            criaTabela.append("</td>");
                            criaTabela.append("</tr>");


                            out.print(criaTabela.toString());
                        }
                    } else {
                        out.print("<tr><td colspan='8'>Nenhum dado a ser mostrado!</td></tr>");
                    }
                %>
                </tbody>
            </fieldset>
        </table>
    </article>
</section>
<footer>

</footer>

</body>
</html>

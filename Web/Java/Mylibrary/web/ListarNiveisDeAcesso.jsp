<%@ page import="br.com.kedge.mylibrary.core.implementation.dao.cAccessLevelDao" %>
<%@ page import="br.com.kedge.mylibrary.domain.cEntityDomain" %>
<%@ page import="br.com.kedge.mylibrary.domain.entities.cAccessLevel" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: geovane95
  Date: 12/10/17
  Time: 02:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Niveis de Acesso</title>
</head>
<body>
<%
    StringBuilder criaTabela = new StringBuilder();
    cEntityDomain objEntityDomain = new cEntityDomain();
    cAccessLevelDao objAccessLevelDao = new cAccessLevelDao();
    List<cAccessLevel> objAccessLevels = new ArrayList<cAccessLevel>((Collection<? extends cAccessLevel>) objAccessLevelDao.retrive(objEntityDomain));
%>
<header class="jumbotron">
    <h1>Niveis de Acesso</h1>
</header>
<nav class="navbar navbar-default">

</nav>
<section>
    <article>
        <table>
            <thead>
            <tr>
                <th>#ID</th>
                <th>Acesso</th>
                <th>Ativo</th>
                <th>Alterar</th>
                <th>Ativar/Inativar</th>
                <th>Consultar</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (cAccessLevel objAL : objAccessLevels) {
                    criaTabela.append("<tr>");
                    criaTabela.append("<td>");
                    criaTabela.append(objAL.getId());
                    criaTabela.append("</td>");
                    criaTabela.append("<td>");
                    criaTabela.append(objAL.getAccess());
                    criaTabela.append("</td>");
                    criaTabela.append("<td>");
                    if (objAL.isActive()) {
                        criaTabela.append("Sim");
                    } else {
                        criaTabela.append("Não");
                    }
                    criaTabela.append("</td>");
                    criaTabela.append("<td>");
                    criaTabela.append("<a href='AccessLevel?id=" + objAL.getId() + "operation=Alterar'><button type='button' class='btn btn-primary'>Alterar</button></a>");
                    criaTabela.append("</td>");
                    criaTabela.append("<td>");
                    if (objAL.isActive()) {
                        criaTabela.append("<a href='AccessLevel?id=" + objAL.getId() + "operation=Inativar'><button type='button' class='btn btn-danger'>Inativar</button></a>");
                    } else {
                        criaTabela.append("<a href='AccessLevel?id=" + objAL.getId() + "operation=Ativar'><button type='button' class='btn btn-warning'>Ativar</button></a>");
                    }
                    criaTabela.append("</td>");
                    criaTabela.append("<td>");
                    criaTabela.append("<a href='AccessLevel?id=" + objAL.getId() + "operation=Consultar'><button type='button' class='btn btn-success'>Consultar</button></a>");
                    criaTabela.append("</td>");
                    criaTabela.append("</tr>");


                    out.print(criaTabela.toString());
                }
            %>
            </tbody>
        </table>
    </article>
</section>
<footer>

</footer>
</body>
</html>

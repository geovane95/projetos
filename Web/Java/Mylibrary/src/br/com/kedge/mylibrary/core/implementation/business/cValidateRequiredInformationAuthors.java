package br.com.kedge.mylibrary.core.implementation.business;

import br.com.kedge.mylibrary.core.iStrategy;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entities.cAuthor;

import java.util.Date;

public class cValidateRequiredInformationAuthors implements iStrategy {
    @Override
    public String process(cEntityDomain entity) {

        if (entity instanceof cAuthor) {
            cAuthor objAuthor = (cAuthor) entity;

            String nome = objAuthor.getName();
            Date dtnasc = objAuthor.getDtBeginLife();
            int books = objAuthor.getBooks();

            if (nome == null || nome.trim().equals("")) {
                return "O nome do Autor é obrigatório!";
            }
            if (dtnasc == null) {
                return "A data de nascimento do Autor é obrigatório!";
            }
            if (books < 0) {
                return "O número de livros do Autor não pode ser menor que zero!";
            }

        } else {
            return "Deve ser registrado um Autor!";
        }

        return null;
    }
}

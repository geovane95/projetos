package br.com.kedge.mylibrary.domain.entities;

import java.util.Date;

public class cSupplier extends cLegalPerson {

    private cNaturalPerson objAccountable;

    public cSupplier() {
    }

    public cSupplier(int id, boolean active, String name, Date dtBeginLife, String email,
                     String password, Date dtRegister, cAccessLevel objAccessLevel,
                     cPhone objPhones, cAddress objAddress, String cnpj, String StateRegister,
                     String MunicipalRegister, String companyName, cNaturalPerson objAccountable) {
        super(id, active, name, dtBeginLife, email, password, dtRegister, objAccessLevel,
                objPhones, objAddress, cnpj, StateRegister, MunicipalRegister, companyName);
        this.setObjAccountable(objAccountable);
    }

    public cNaturalPerson getObjAccountable() {
        return objAccountable;
    }

    public void setObjAccountable(cNaturalPerson objResponsavel) {
        this.objAccountable = objAccountable;
    }
}

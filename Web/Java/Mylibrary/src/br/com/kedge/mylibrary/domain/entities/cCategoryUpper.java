package br.com.kedge.mylibrary.domain.entities;

import java.util.List;

public class cCategoryUpper extends cCategories {

    private List<cCategories> objCategoriesLower;

    public cCategoryUpper() {
    }

    public cCategoryUpper(int id, boolean active, String name, List<cCategories> objCategoriesLower) {
        super(id, active, name);

        this.setObjCategoriesLower(objCategoriesLower);
    }

    public List getObjCategoriesLower() {
        return objCategoriesLower;
    }

    public void setObjCategoriesLower(List objCategoriesLower) {
        this.objCategoriesLower = objCategoriesLower;
    }
}

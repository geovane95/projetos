package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cCountry extends cEntityDomain {

    private String country;

    public cCountry() {
    }

    public cCountry(int id, boolean active, String country) {
        super(id, active);
        this.setCountry(country);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}

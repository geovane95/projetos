package br.com.kedge.mylibrary.domain.entities;

public class cStreet {

    private String name;

    private cTypeStreet objTypeStreet;

    public cStreet() {
    }

    public cStreet(String name, cTypeStreet objTypeStreet) {
        this.setName(name);
        this.setObjTypeStreet(objTypeStreet);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public cTypeStreet getObjTypeStreet() {
        return objTypeStreet;
    }

    public void setObjTypeStreet(cTypeStreet objTypeStreet) {
        this.objTypeStreet = objTypeStreet;
    }
}

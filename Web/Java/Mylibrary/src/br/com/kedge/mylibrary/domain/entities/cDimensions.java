package br.com.kedge.mylibrary.domain.entities;

public class cDimensions {

    private double height;

    private double witdh;

    private double weight;

    private double depth;

    public cDimensions(double height, double witdh, double weight, double depth) {
        this.setHeight(height);
        this.setWitdh(witdh);
        this.setWeight(weight);
        this.setDepth(depth);
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWitdh() {
        return witdh;
    }

    public void setWitdh(double witdh) {
        this.witdh = witdh;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }
}

package br.com.kedge.mylibrary.domain.entities;

import java.util.Date;

public class cNaturalPerson extends acCustomer {

    private String cpf;

    private String generalRegister;

    cNaturalPerson() {
    }

    cNaturalPerson(int id, boolean active, String name, Date dtBeginLife, String email,
                   String password, Date dtRegister, cAccessLevel objAccessLevel,
                   cPhone objPhones, cAddress objAddress, String cpf, String generalRegister) {
        super(id, active, name, dtBeginLife, email, password, dtRegister, objAccessLevel,
                objPhones, objAddress);

        this.setCpf(cpf);

        this.setGeneralRegister(generalRegister);
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getGeneralRegister() {
        return generalRegister;
    }

    public void setGeneralRegister(String generalRegister) {
        this.generalRegister = generalRegister;
    }
}

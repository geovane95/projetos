package br.com.kedge.mylibrary.core.implementation.control;

import br.com.kedge.mylibrary.core.aplication.cResult;
import br.com.kedge.mylibrary.core.iDao;
import br.com.kedge.mylibrary.core.iFacade;
import br.com.kedge.mylibrary.core.iStrategy;
import br.com.kedge.mylibrary.core.implementation.business.*;
import br.com.kedge.mylibrary.core.implementation.dao.*;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entities.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class cFacade implements iFacade {

    /**
     * Mapa de DAOS, será indexado pelo nome da entidade
     * O valor é uma instância do DAO para uma dada entidade;
     */
    private Map<String, iDao> daos;

    /**
     * Mapa para conter as regras de negócio de todas operações por entidade;
     * O valor é um mapa que de regras de negócio indexado pela operação
     */
    private Map<String, Map<String, List<iStrategy>>> rns;

    private cResult objResult;

    public cFacade() {
        /* Intânciando o Map de DAOS */
        daos = new HashMap<String, iDao>();

        /* Intânciando o Map de Regras de Negócio */
        rns = new HashMap<String, Map<String, List<iStrategy>>>();

        /* Criando instâncias dos DAOs a serem utilizados*/
        cAddressDao objAddressDao = new cAddressDao();
        cAccessLevelDao objAccessLevelDao = new cAccessLevelDao();
        cAuthorsDao objAuthorsDao = new cAuthorsDao();
        cBannerCardsDao objBannerCardsDao = new cBannerCardsDao();
        cBooksDao objBooksDao = new cBooksDao();
        cCatBooksDao objCatBooksDao = new cCatBooksDao();
        cCategoriesDao objCategoriesDao = new cCategoriesDao();
        cCityDao objCityDao = new cCityDao();
        cCountryDao objCountryDao = new cCountryDao();
        cCreditCardsLPDao objCreditCardsLPDao = new cCreditCardsLPDao();
        cCreditCardsNPDao objCreditCardsNPDao = new cCreditCardsNPDao();
        cEditionsDao objEditionsDao = new cEditionsDao();
        cEditorsDao objEditorsDao = new cEditorsDao();
        cHistoryDao objHistoryDao = new cHistoryDao();
        cLegalPersonDao objLegalPersonDao = new cLegalPersonDao();
        cLocationDao objLocationDao = new cLocationDao();
        cNaturalPersonDao objNaturalPersonDao = new cNaturalPersonDao();
        cOperationsDao objOperationsDao = new cOperationsDao();
        cPhoneDao objPhoneDao = new cPhoneDao();
        cPricingGroupDao objPricingGroupDao = new cPricingGroupDao();
        cStateDao objStateDao = new cStateDao();
        cSuppliersDao objSuppliersDao = new cSuppliersDao();
        cTypeAddressDao objTypeAddressDao = new cTypeAddressDao();
        cTypeHouseDao objTypeHouseDao = new cTypeHouseDao();
        cTypePhoneDao objTypePhoneDao = new cTypePhoneDao();

        /* Adicionando cada dao no MAP indexando pelo nome da classe */
        daos.put(cAddress.class.getName(), objAddressDao);
        daos.put(cAccessLevel.class.getName(), objAccessLevelDao);
        daos.put(cAuthor.class.getName(), objAuthorsDao);
        //daos.put(cBannerCard.class.getName(), objBannerCardsDao);
        daos.put(cBooks.class.getName(), objBooksDao);
        daos.put(cCategories.class.getName(), objCategoriesDao);
        daos.put(cCity.class.getName(), objCityDao);
        daos.put(cCountry.class.getName(), objCountryDao);
        //daos.put(cCreditCardLP.class.getName(), objCreditCardsLPDao);
        //daos.put(cCreditCardNP.class.getName(), objCreditCardsNPDao);
        daos.put(cEdition.class.getName(), objEditionsDao);
        daos.put(cEditors.class.getName(), objEditorsDao);
        //daos.put(cHistory.class.getName(), objHistoryDao);
        daos.put(cLegalPerson.class.getName(), objLegalPersonDao);
        daos.put(cLocation.class.getName(), objLocationDao);
        daos.put(cNaturalPerson.class.getName(), objNaturalPersonDao);
        //daos.put(cOperation.class.getName(), objOperationsDao);
        daos.put(cPhone.class.getName(), objPhoneDao);
        daos.put(cPricingGroup.class.getName(), objPricingGroupDao);
        daos.put(cState.class.getName(), objStateDao);
        daos.put(cSupplier.class.getName(), objSuppliersDao);
        daos.put(cTypeAddress.class.getName(), objTypeAddressDao);
        daos.put(cTypeHouse.class.getName(), objTypeHouseDao);
        daos.put(cTypePhone.class.getName(), objTypePhoneDao);

        /* Criando instâncias de regras de negócio a serem utilizados*/
        cValidateRequiredInformationAccessLevel objVRIAccessLevel = new cValidateRequiredInformationAccessLevel();
        cValidateRequiredInformationAddress objVRIAddress = new cValidateRequiredInformationAddress();
        cValidateRequiredInformationAuthors objVRIAuthors = new cValidateRequiredInformationAuthors();
        cValidateRequiredInformationBannerCards objVRIBannerCards = new cValidateRequiredInformationBannerCards();
        cValidateRequiredInformationCatBooks objVRICatBooks = new cValidateRequiredInformationCatBooks();
        cValidateRequiredInformationCategories objVRICategories = new cValidateRequiredInformationCategories();
        cValidateRequiredInformationCity objVRICity = new cValidateRequiredInformationCity();
        cValidateRequiredInformationCountry objVRICountry = new cValidateRequiredInformationCountry();
        cValidateRequiredInformationCreditCardsLP objVRICreditCardsLP = new cValidateRequiredInformationCreditCardsLP();
        cValidateRequiredInformationCreditCardsNP objVRICreditCardsNP = new cValidateRequiredInformationCreditCardsNP();
        cValidateRequiredInformationEditions objVRIEditions = new cValidateRequiredInformationEditions();
        cValidateRequiredInformationEditors objVRIEditors = new cValidateRequiredInformationEditors();
        cValidateRequiredInformationHistory objVRIHistory = new cValidateRequiredInformationHistory();
        cValidateRequiredInformationLegalPerson objVRILegalPerson = new cValidateRequiredInformationLegalPerson();
        cValidateRequiredInformationLocation objVRILocation = new cValidateRequiredInformationLocation();
        cValidateRequiredInformationNaturalPerson objVRINaturalPerson = new cValidateRequiredInformationNaturalPerson();
        cValidateRequiredInformationOperations objVRIOperations = new cValidateRequiredInformationOperations();
        cValidateRequiredInformationPhone objVRIPhone = new cValidateRequiredInformationPhone();
        cValidateRequiredInformationPricingGroup objVRIPricingGroup = new cValidateRequiredInformationPricingGroup();
        cValidateRequiredInformationState objVRIState = new cValidateRequiredInformationState();
        cValidateRequiredInformationSuppliers objVRISuppliers = new cValidateRequiredInformationSuppliers();
        cValidateRequiredInformationTypeAddress objVRITypeAddress = new cValidateRequiredInformationTypeAddress();
        cValidateRequiredInformationTypeHouse objVRITypeHouse = new cValidateRequiredInformationTypeHouse();
        cValidateRequiredInformationTypePhone objVRITypePhone = new cValidateRequiredInformationTypePhone();
        cValidateRequiredInformationTypeStreet objVRITypeStreet = new cValidateRequiredInformationTypeStreet();

        /* Criando uma lista para conter as regras de negócio
         * quando a operação for create
		 */
        List<iStrategy> rnsCreateAccessLevel = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateAddress = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateAuthors = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateBannerCards = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateCatBooks = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateCategories = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateCity = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateCountry = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateCreditCardsLP = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateCreditCardsNP = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateEditions = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateEditors = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateHistory = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateLegalPerson = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateLocation = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateNaturalPerson = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateOperations = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreatePhone = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreatePricingGroup = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateState = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateSuppliers = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateTypeAddress = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateTypeHouse = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateTypePhone = new ArrayList<iStrategy>();
        List<iStrategy> rnsCreateTypeStreet = new ArrayList<iStrategy>();


        /* Criando uma lista para conter as regras de negócio
         * quando a operação for update
		 */
        List<iStrategy> rnsUpdateAccessLevel = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateAddress = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateAuthors = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateBannerCards = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateCatBooks = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateCategories = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateCity = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateCountry = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateCreditCardsLP = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateCreditCardsNP = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateEditions = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateEditors = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateHistory = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateLegalPerson = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateLocation = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateNaturalPerson = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateOperations = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdatePhone = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdatePricingGroup = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateState = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateSuppliers = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateTypeAddress = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateTypeHouse = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateTypePhone = new ArrayList<iStrategy>();
        List<iStrategy> rnsUpdateTypeStreet = new ArrayList<iStrategy>();


        /* Criando uma lista para conter as regras de negócio
         * quando a operação for drop
		 */
        List<iStrategy> rnsDropAccessLevel = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropAddress = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropAuthors = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropBannerCards = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropCatBooks = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropCategories = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropCity = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropCountry = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropCreditCardsLP = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropCreditCardsNP = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropEditions = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropEditors = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropHistory = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropLegalPerson = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropLocation = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropNaturalPerson = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropOperations = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropPhone = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropPricingGroup = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropState = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropSuppliers = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropTypeAddress = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropTypeHouse = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropTypePhone = new ArrayList<iStrategy>();
        List<iStrategy> rnsDropTypeStreet = new ArrayList<iStrategy>();


        /* Criando uma lista para conter as regras de negócio
         * quando a operação for inactive
		 */
        List<iStrategy> rnsInactiveAccessLevel = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveAddress = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveAuthors = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveBannerCards = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveCatBooks = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveCategories = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveCity = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveCountry = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveCreditCardsLP = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveCreditCardsNP = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveEditions = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveEditors = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveHistory = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveLegalPerson = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveLocation = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveNaturalPerson = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveOperations = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactivePhone = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactivePricingGroup = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveState = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveSuppliers = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveTypeAddress = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveTypeHouse = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveTypePhone = new ArrayList<iStrategy>();
        List<iStrategy> rnsInactiveTypeStreet = new ArrayList<iStrategy>();


        /* Criando uma lista para conter as regras de negócio
         * quando a operação for active
		 */
        List<iStrategy> rnsActiveAccessLevel = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveAddress = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveAuthors = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveBannerCards = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveCatBooks = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveCategories = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveCity = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveCountry = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveCreditCardsLP = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveCreditCardsNP = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveEditions = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveEditors = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveHistory = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveLegalPerson = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveLocation = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveNaturalPerson = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveOperations = new ArrayList<iStrategy>();
        List<iStrategy> rnsActivePhone = new ArrayList<iStrategy>();
        List<iStrategy> rnsActivePricingGroup = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveState = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveSuppliers = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveTypeAddress = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveTypeHouse = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveTypePhone = new ArrayList<iStrategy>();
        List<iStrategy> rnsActiveTypeStreet = new ArrayList<iStrategy>();

        /* Adicionando as regras a serem utilizadas na operação salvar do fornecedor*/
        rnsCreateAccessLevel.add(objVRIAccessLevel);
        rnsCreateAddress.add(objVRIAddress);
        rnsCreateAuthors.add(objVRIAuthors);
        rnsCreateBannerCards.add(objVRIBannerCards);
        rnsCreateCatBooks.add(objVRICatBooks);
        rnsCreateCategories.add(objVRICategories);
        rnsCreateCity.add(objVRICity);
        rnsCreateCountry.add(objVRICountry);
        rnsCreateCreditCardsLP.add(objVRICreditCardsLP);
        rnsCreateCreditCardsNP.add(objVRICreditCardsNP);
        rnsCreateEditions.add(objVRIEditions);
        rnsCreateEditors.add(objVRIEditors);
        rnsCreateHistory.add(objVRIHistory);
        rnsCreateLegalPerson.add(objVRILegalPerson);
        rnsCreateLocation.add(objVRILocation);
        rnsCreateNaturalPerson.add(objVRINaturalPerson);
        rnsCreateOperations.add(objVRIOperations);
        rnsCreatePhone.add(objVRIPhone);
        rnsCreatePricingGroup.add(objVRIPricingGroup);
        rnsCreateState.add(objVRIState);
        rnsCreateSuppliers.add(objVRISuppliers);
        rnsCreateTypeAddress.add(objVRITypeAddress);
        rnsCreateTypeHouse.add(objVRITypeHouse);
        rnsCreateTypePhone.add(objVRITypePhone);
        rnsCreateTypeStreet.add(objVRITypeStreet);

        /* Cria o mapa que poderá conter todas as listas de regras de negócio específica
         * por operação  do access level
		 */
        Map<String, List<iStrategy>> rnsAccessLevel = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsAddress = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsAuthors = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsBannerCards = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsCatBooks = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsCategories = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsCity = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsCountry = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsCreditCardsNP = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsCreditCardsLP = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsEditions = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsEditors = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsHistory = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsLegalPerson = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsLocation = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsNaturalPerson = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsOperations = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsPhone = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsPricingGroup = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsState = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsSuppliers = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsTypeAddress = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsTypeHouse = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsTypePhone = new HashMap<String, List<iStrategy>>();
        Map<String, List<iStrategy>> rnsTypeStreet = new HashMap<String, List<iStrategy>>();

        /*
         * Adiciona a lista de regras de todas as operações no mapa de Autores
		 */

        rnsAuthors.put("Salvar", rnsCreateAuthors);
        rnsAuthors.put("Alterar", rnsUpdateAuthors);
        rnsAuthors.put("Excluir", rnsDropAuthors);
        rnsAuthors.put("Inativar", rnsInactiveAuthors);
        rnsAuthors.put("Ativar", rnsActiveAuthors);

        rnsAccessLevel.put("Salvar", rnsCreateAccessLevel);
        rnsAddress.put("Salvar", rnsCreateAddress);
        rnsBannerCards.put("Salvar", rnsCreateBannerCards);
        rnsCatBooks.put("Salvar", rnsCreateCatBooks);
        rnsCategories.put("Salvar", rnsCreateCategories);
        rnsCity.put("Salvar", rnsCreateCity);
        rnsCountry.put("Salvar", rnsCreateCountry);
        rnsCreditCardsNP.put("Salvar", rnsCreateCreditCardsNP);
        rnsCreditCardsLP.put("Salvar", rnsCreateCreditCardsLP);
        rnsEditions.put("Salvar", rnsCreateEditions);
        rnsEditors.put("Salvar", rnsCreateEditors);
        rnsHistory.put("Salvar", rnsCreateHistory);
        rnsLegalPerson.put("Salvar", rnsCreateLegalPerson);
        rnsLocation.put("Salvar", rnsCreateLocation);
        rnsNaturalPerson.put("Salvar", rnsCreateNaturalPerson);
        rnsOperations.put("Salvar", rnsCreateOperations);
        rnsPhone.put("Salvar", rnsCreatePhone);
        rnsPricingGroup.put("Salvar", rnsCreatePricingGroup);
        rnsState.put("Salvar", rnsCreateState);
        rnsSuppliers.put("Salvar", rnsCreateSuppliers);
        rnsTypeAddress.put("Salvar", rnsCreateTypeAddress);
        rnsTypeHouse.put("Salvar", rnsCreateTypeHouse);
        rnsTypePhone.put("Salvar", rnsCreateTypePhone);
        rnsTypeStreet.put("Salvar", rnsCreateTypeStreet);

/*
        rnsAccessLevel .put("Alterar", rnsUpdateAccessLevel );
        rnsAddress     .put("Alterar", rnsUpdateAddress     );
        rnsBannerCards .put("Alterar", rnsUpdateBannerCards );
        rnsCatBooks    .put("Alterar", rnsUpdateCatBooks    );
        rnsCategories  .put("Alterar", rnsUpdateCategories  );
        rnsCity        .put("Alterar", rnsUpdateCity        );
        rnsCountry     .put("Alterar", rnsUpdateCountry     );
        rnsCreditCardsNP.put("Alterar", rnsUpdateCreditCardsNP);
        rnsCreditCardsLP.put("Alterar", rnsUpdateCreditCardsLP);
        rnsEditions    .put("Alterar", rnsUpdateEditions    );
        rnsEditors     .put("Alterar", rnsUpdateEditors     );
        rnsHistory     .put("Alterar", rnsUpdateHistory     );
        rnsLegalPerson .put("Alterar", rnsUpdateLegalPerson );
        rnsLocation    .put("Alterar", rnsUpdateLocation    );
        rnsNaturalPerson.put("Alterar", rnsUpdateNaturalPerson);
        rnsOperations  .put("Alterar", rnsUpdateOperations  );
        rnsPhone       .put("Alterar", rnsUpdatePhone       );
        rnsPricingGroup.put("Alterar", rnsUpdatePricingGroup);
        rnsState       .put("Alterar", rnsUpdateState       );
        rnsSuppliers   .put("Alterar", rnsUpdateSuppliers   );
        rnsTypeAddress .put("Alterar", rnsUpdateTypeAddress );
        rnsTypeHouse   .put("Alterar", rnsUpdateTypeHouse   );
        rnsTypePhone   .put("Alterar", rnsUpdateTypePhone   );
        rnsTypeStreet  .put("Alterar", rnsUpdateTypeStreet  );*/


/*
        rnsAccessLevel .put("Excluir", rnsDropAccessLevel );
        rnsAddress     .put("Excluir", rnsDropAddress     );
        rnsBannerCards .put("Excluir", rnsDropBannerCards );
        rnsCatBooks    .put("Excluir", rnsDropCatBooks    );
        rnsCategories  .put("Excluir", rnsDropCategories  );
        rnsCity        .put("Excluir", rnsDropCity        );
        rnsCountry     .put("Excluir", rnsDropCountry     );
        rnsCreditCardsNP.put("Excluir", rnsDropCreditCardsNP);
        rnsCreditCardsLP.put("Excluir", rnsDropCreditCardsLP);
        rnsEditions    .put("Excluir", rnsDropEditions    );
        rnsEditors     .put("Excluir", rnsDropEditors     );
        rnsHistory     .put("Excluir", rnsDropHistory     );
        rnsLegalPerson .put("Excluir", rnsDropLegalPerson );
        rnsLocation    .put("Excluir", rnsDropLocation    );
        rnsNaturalPerson.put("Excluir", rnsDropNaturalPerson);
        rnsOperations  .put("Excluir", rnsDropOperations  );
        rnsPhone       .put("Excluir", rnsDropPhone       );
        rnsPricingGroup.put("Excluir", rnsDropPricingGroup);
        rnsState       .put("Excluir", rnsDropState       );
        rnsSuppliers   .put("Excluir", rnsDropSuppliers   );
        rnsTypeAddress .put("Excluir", rnsDropTypeAddress );
        rnsTypeHouse   .put("Excluir", rnsDropTypeHouse   );
        rnsTypePhone   .put("Excluir", rnsDropTypePhone   );
        rnsTypeStreet  .put("Excluir", rnsDropTypeStreet  );*/


/*
        rnsAccessLevel .put("Inativar", rnsInactiveAccessLevel );
        rnsAddress     .put("Inativar", rnsInactiveAddress     );
        rnsBannerCards .put("Inativar", rnsInactiveBannerCards );
        rnsCatBooks    .put("Inativar", rnsInactiveCatBooks    );
        rnsCategories  .put("Inativar", rnsInactiveCategories  );
        rnsCity        .put("Inativar", rnsInactiveCity        );
        rnsCountry     .put("Inativar", rnsInactiveCountry     );
        rnsCreditCardsNP.put("Inativar", rnsInactiveCreditCardsNP);
        rnsCreditCardsLP.put("Inativar", rnsInactiveCreditCardsLP);
        rnsEditions    .put("Inativar", rnsInactiveEditions    );
        rnsEditors     .put("Inativar", rnsInactiveEditors     );
        rnsHistory     .put("Inativar", rnsInactiveHistory     );
        rnsLegalPerson .put("Inativar", rnsInactiveLegalPerson );
        rnsLocation    .put("Inativar", rnsInactiveLocation    );
        rnsNaturalPerson.put("Inativar", rnsInactiveNaturalPerson);
        rnsOperations  .put("Inativar", rnsInactiveOperations  );
        rnsPhone       .put("Inativar", rnsInactivePhone       );
        rnsPricingGroup.put("Inativar", rnsInactivePricingGroup);
        rnsState       .put("Inativar", rnsInactiveState       );
        rnsSuppliers   .put("Inativar", rnsInactiveSuppliers   );
        rnsTypeAddress .put("Inativar", rnsInactiveTypeAddress );
        rnsTypeHouse   .put("Inativar", rnsInactiveTypeHouse   );
        rnsTypePhone   .put("Inativar", rnsInactiveTypePhone   );
        rnsTypeStreet  .put("Inativar", rnsInactiveTypeStreet  );*/


/*
        rnsAccessLevel .put("Ativar", rnsActiveAccessLevel );
        rnsAddress     .put("Ativar", rnsActiveAddress     );
        rnsBannerCards .put("Ativar", rnsActiveBannerCards );
        rnsCatBooks    .put("Ativar", rnsActiveCatBooks    );
        rnsCategories  .put("Ativar", rnsActiveCategories  );
        rnsCity        .put("Ativar", rnsActiveCity        );
        rnsCountry     .put("Ativar", rnsActiveCountry     );
        rnsCreditCardsNP.put("Ativar", rnsActiveCreditCardsNP);
        rnsCreditCardsLP.put("Ativar", rnsActiveCreditCardsLP);
        rnsEditions    .put("Ativar", rnsActiveEditions    );
        rnsEditors     .put("Ativar", rnsActiveEditors     );
        rnsHistory     .put("Ativar", rnsActiveHistory     );
        rnsLegalPerson .put("Ativar", rnsActiveLegalPerson );
        rnsLocation    .put("Ativar", rnsActiveLocation    );
        rnsNaturalPerson.put("Ativar", rnsActiveNaturalPerson);
        rnsOperations  .put("Ativar", rnsActiveOperations  );
        rnsPhone       .put("Ativar", rnsActivePhone       );
        rnsPricingGroup.put("Ativar", rnsActivePricingGroup);
        rnsState       .put("Ativar", rnsActiveState       );
        rnsSuppliers   .put("Ativar", rnsActiveSuppliers   );
        rnsTypeAddress .put("Ativar", rnsActiveTypeAddress );
        rnsTypeHouse   .put("Ativar", rnsActiveTypeHouse   );
        rnsTypePhone   .put("Ativar", rnsActiveTypePhone   );
        rnsTypeStreet  .put("Ativar", rnsActiveTypeStreet  );*/


        /* Adiciona o mapa com as regras indexadas pelas operações no mapa geral indexado
         * pelo nome da entidade
		 */
        rns.put(cAccessLevel.class.getName(), rnsAccessLevel);
        rns.put(cAddress.class.getName(), rnsAddress);
        rns.put(cAuthor.class.getName(), rnsAuthors);
        //rns.put(cBannerCards .class.getName(), rnsBannerCards );
        //rns.put(cCatBooks    .class.getName(), rnsCatBooks    );
        rns.put(cCategories.class.getName(), rnsCategories);
        rns.put(cCity.class.getName(), rnsCity);
        rns.put(cCountry.class.getName(), rnsCountry);
        //rns.put(cCreditCardsNP.class.getName(), rnsCreditCardsNP);
        //rns.put(cCreditCardsLP.class.getName(), rnsCreditCardsLP);
        rns.put(cEdition.class.getName(), rnsEditions);
        rns.put(cEditors.class.getName(), rnsEditors);
        //rns.put(cHistory     .class.getName(), rnsHistory     );
        rns.put(cLegalPerson.class.getName(), rnsLegalPerson);
        rns.put(cLocation.class.getName(), rnsLocation);
        rns.put(cNaturalPerson.class.getName(), rnsNaturalPerson);
        //rns.put(cOperation  .class.getName(), rnsOperations  );
        rns.put(cPhone.class.getName(), rnsPhone);
        rns.put(cPricingGroup.class.getName(), rnsPricingGroup);
        rns.put(cState.class.getName(), rnsState);
        rns.put(cSupplier.class.getName(), rnsSuppliers);
        rns.put(cTypeAddress.class.getName(), rnsTypeAddress);
        rns.put(cTypeHouse.class.getName(), rnsTypeHouse);
        rns.put(cTypePhone.class.getName(), rnsTypePhone);
        rns.put(cTypeStreet.class.getName(), rnsTypeStreet);


    }

    @Override
    public cResult create(cEntityDomain objEntityDomain) {
        objResult = new cResult();

        String nmClass = objEntityDomain.getClass().getName();

        String msg = executeRules(objEntityDomain, "Salvar");

        if (msg == null) {
            iDao dao = daos.get(nmClass);
            try {
                dao.create(objEntityDomain);
                List<cEntityDomain> objEntitiesDomain = new ArrayList<cEntityDomain>();
                objEntitiesDomain.add(objEntityDomain);
                objResult.setEntities(objEntitiesDomain);
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Erro de SQL ao obter as entidades!");
                objResult.setMsg("Não foi possivel realizar o registro!");
            }
        } else {
            objResult.setMsg(msg);
        }
        return objResult;
    }

    @Override
    public cResult update(cEntityDomain objEntityDomain) {

        objResult = new cResult();

        String nmClass = objEntityDomain.getClass().getName();

        String msg = executeRules(objEntityDomain, "Alterar");

        if (msg == null) {
            iDao dao = daos.get(nmClass);
            try {
                dao.update(objEntityDomain);
                List<cEntityDomain> objEntitiesDomain = new ArrayList<cEntityDomain>();
                objEntitiesDomain.add(objEntityDomain);
                objResult.setEntities(objEntitiesDomain);
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Erro ao executar a alteração de um registro!");
                objResult.setMsg("Não foi possivel realizar a alteração do registro!");
            }
        } else {
            objResult.setMsg(msg);
        }

        return objResult;
    }

    @Override
    public cResult drop(cEntityDomain objEntityDomain) {

        objResult = new cResult();

        String nmClass = objEntityDomain.getClass().getName();

        String msg = executeRules(objEntityDomain, "Excluir");

        if (msg == null) {
            iDao dao = daos.get(nmClass);
            try {
                dao.drop(objEntityDomain);
                List<cEntityDomain> objEntitiesDomain = new ArrayList<cEntityDomain>();
                objEntitiesDomain.add(objEntityDomain);
                objResult.setEntities(objEntitiesDomain);
            } catch (SQLException e) {
                e.printStackTrace();
                objResult.setMsg("Não foi excluir o registro!");
                System.out.println("Erro ao excluir um registro!");
            }
        } else {
            objResult.setMsg(msg);
        }

        return objResult;
    }

    @Override
    public cResult retrive(cEntityDomain objEntityDomain) {

        objResult = new cResult();

        String nmClass = objEntityDomain.getClass().getName();

        String msg = executeRules(objEntityDomain, "Consultar");

        if (msg == null) {
            iDao dao = daos.get(nmClass);
            try {
                objResult.setEntities(dao.retrive(objEntityDomain));
            } catch (SQLException e) {
                e.printStackTrace();
                objResult.setMsg("Não foi possivel realizar a consulta!");
                System.out.println("Erro ao efetuar a consulta!");
            }
        } else {
            objResult.setMsg(msg);
        }

        return objResult;
    }

    @Override
    public cResult view(cEntityDomain objEntityDomain) {

        objResult = new cResult();

        objResult.setEntities(new ArrayList<cEntityDomain>(1));
        objResult.getEntities().add(objEntityDomain);
        return objResult;
    }

    @Override
    public cResult active(cEntityDomain objEntityDomain) {

        objResult = new cResult();

        String nmClass = objEntityDomain.getClass().getName();

        String msg = executeRules(objEntityDomain, "Ativar");

        if (msg == null) {
            iDao dao = daos.get(nmClass);
            try {
                dao.active(objEntityDomain);
                List<cEntityDomain> objEntitiesDomain = new ArrayList<cEntityDomain>();
                objEntitiesDomain.add(objEntityDomain);
                objResult.setEntities(objEntitiesDomain);
            } catch (SQLException e) {
                e.printStackTrace();
                objResult.setMsg("Não foi excluir o registro!");
                System.out.println("Erro ao excluir um registro!");
            }
        } else {
            objResult.setMsg(msg);
        }

        return objResult;
    }

    @Override
    public cResult inactive(cEntityDomain objEntityDomain) {

        objResult = new cResult();

        String nmClass = objEntityDomain.getClass().getName();

        String msg = executeRules(objEntityDomain, "Inativar");

        if (msg == null) {
            iDao dao = daos.get(nmClass);
            try {
                dao.inactive(objEntityDomain);
                List<cEntityDomain> objEntitiesDomain = new ArrayList<cEntityDomain>();
                objEntitiesDomain.add(objEntityDomain);
                objResult.setEntities(objEntitiesDomain);
            } catch (SQLException e) {
                e.printStackTrace();
                objResult.setMsg("Não foi possivel inativar o registro!");
                System.out.println("Erro ao inativar um registro!");
            }
        } else {
            objResult.setMsg(msg);
        }

        return objResult;
    }

    private String executeRules(cEntityDomain objEntityDomain, String operation) {

        String nmClass = objEntityDomain.getClass().getName();

        StringBuilder msg = new StringBuilder();

        Map<String, List<iStrategy>> rulesOperation = rns.get(nmClass);

        if (rulesOperation != null) {
            List<iStrategy> rules = rulesOperation.get(operation);

            if (rules != null) {
                for (iStrategy s : rules) {
                    String m = s.process(objEntityDomain);

                    if (m != null) {
                        msg.append(m);
                        msg.append("\n");
                    }
                }
            }
        }

        if (msg.length() > 0) {
            return msg.toString();
        } else {
            return null;
        }
    }
}
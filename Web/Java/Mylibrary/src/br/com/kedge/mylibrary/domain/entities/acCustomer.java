package br.com.kedge.mylibrary.domain.entities;

import java.util.Date;

public class acCustomer extends cUser {

    private cPhone objPhones;

    private cAddress objAddress;

    public acCustomer() {
    }

    public acCustomer(int id, boolean active, String name, Date dtBeginLife, String email,
                      String password, Date dtRegister, cAccessLevel objAccessLevel,
                      cPhone objPhones, cAddress objAddress) {
        super(id, active, name, dtBeginLife, email, password, dtRegister, objAccessLevel);

        this.setObjPhones(objPhones);

        this.setObjAddress(objAddress);
    }

    public cPhone getObjPhones() {
        return objPhones;
    }

    public void setObjPhones(cPhone objPhones) {
        this.objPhones = objPhones;
    }

    public cAddress getObjAddress() {
        return objAddress;
    }

    public void setObjAddress(cAddress objAddress) {
        this.objAddress = objAddress;
    }
}

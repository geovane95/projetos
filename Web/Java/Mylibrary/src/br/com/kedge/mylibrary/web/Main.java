package br.com.kedge.mylibrary.web;

import br.com.kedge.mylibrary.core.util.cConnection;
import br.com.kedge.mylibrary.domain.entities.cAccessLevel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Main {
    void main(String args[]) {
        Connection conn = cConnection.getConnection();
        System.out.println("Conexão realizada com sucesso!");
        cAccessLevel objAccessLevel = new cAccessLevel();
        String sql = "SELECT * FROM accesslevel";
        try {
            PreparedStatement pst = conn.prepareStatement(sql.toString());
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                System.out.println("ID:" + rs.getInt(1) + ", Acesso:"
                        + rs.getString(2) + ", Ativo:" + rs.getBoolean(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

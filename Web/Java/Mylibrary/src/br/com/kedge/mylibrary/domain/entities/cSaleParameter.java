package br.com.kedge.mylibrary.domain.entities;

public class cSaleParameter {

    private int timeRecurrence;

    private int minNumberBooks;

    public cSaleParameter() {
    }

    public cSaleParameter(int timeRecurrence, int minNumberBooks) {
        this.setTimeRecurrence(timeRecurrence);
        this.setMinNumberBooks(minNumberBooks);
    }


    public int getTimeRecurrence() {
        return timeRecurrence;
    }

    public void setTimeRecurrence(int timeRecurrence) {
        this.timeRecurrence = timeRecurrence;
    }

    public int getMinNumberBooks() {
        return minNumberBooks;
    }

    public void setMinNumberBooks(int minNumberBooks) {
        this.minNumberBooks = minNumberBooks;
    }
}

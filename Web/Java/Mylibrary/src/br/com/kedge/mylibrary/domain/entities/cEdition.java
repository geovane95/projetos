package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cEdition extends cEntityDomain {

    private String edition;

    private int year;

    private int numPages;

    private int barCode;

    private double value;

    private cEditors objEditor;

    private cStockParameter objStockParameter;

    private cSaleParameter objSaleParameter;

    private cISBN objISBN;

    private cDimensions objDimensions;

    private cPricingGroup objPricingGroup;

    private cBooks objBook;

    public cEdition() {

    }

    public cEdition(int id, boolean active, String edition, int year, int numPages, int barCode,
                    double value, cEditors objEditor, cStockParameter objStockParameter,
                    cSaleParameter objSaleParameter, cISBN objISBN, cDimensions objDimensions,
                    cPricingGroup objPricingGroup, cBooks objBook) {
        super(id, active);

        this.setEdition(edition);

        this.setYear(year);

        this.setNumPages(numPages);

        this.setBarCode(barCode);

        this.setValue(value);

        this.setObjEditor(objEditor);

        this.setObjStockParameter(objStockParameter);

        this.setObjSaleParameter(objSaleParameter);

        this.setObjISBN(objISBN);

        this.setObjDimensions(objDimensions);

        this.setObjPricingGroup(objPricingGroup);

        this.setObjBook(objBook);
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getNumPages() {
        return numPages;
    }

    public void setNumPages(int numPages) {
        this.numPages = numPages;
    }

    public int getBarCode() {
        return barCode;
    }

    public void setBarCode(int barCode) {
        this.barCode = barCode;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public cEditors getObjEditor() {
        return objEditor;
    }

    public void setObjEditor(cEditors objEditor) {
        this.objEditor = objEditor;
    }

    public cStockParameter getObjStockParameter() {
        return objStockParameter;
    }

    public void setObjStockParameter(cStockParameter objStockParameter) {
        this.objStockParameter = objStockParameter;
    }

    public cSaleParameter getObjSaleParameter() {
        return objSaleParameter;
    }

    public void setObjSaleParameter(cSaleParameter objSaleParameter) {
        this.objSaleParameter = objSaleParameter;
    }

    public cISBN getObjISBN() {
        return objISBN;
    }

    public void setObjISBN(cISBN objISBN) {
        this.objISBN = objISBN;
    }

    public cDimensions getObjDimensions() {
        return objDimensions;
    }

    public void setObjDimensions(cDimensions objDimensions) {
        this.objDimensions = objDimensions;
    }

    public cPricingGroup getObjPricingGroup() {
        return objPricingGroup;
    }

    public void setObjPricingGroup(cPricingGroup objPricingGroup) {
        this.objPricingGroup = objPricingGroup;
    }

    public cBooks getObjBook() {
        return objBook;
    }

    public void setObjBook(cBooks objBook) {
        this.objBook = objBook;
    }
}

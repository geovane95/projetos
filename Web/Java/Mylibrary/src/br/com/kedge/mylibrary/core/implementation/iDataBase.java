package br.com.kedge.mylibrary.core.implementation;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public interface iDataBase {
    public static boolean retriveDataBase(String driver,
                                          String address, String port, String database, String nmDatabase,
                                          String user, String password) {
        List<String> list = new ArrayList<String>();
        String listofDatabases = null;
        try {
            Class.forName(driver);
            Connection con = DriverManager.getConnection("jdbc:" + database + "://" + address + ":" + port, user, password);
            Statement st = con.createStatement();
            DatabaseMetaData meta = con.getMetaData();
            ResultSet rs = meta.getCatalogs();
            while (rs.next()) {
                listofDatabases = rs.getString("TABLE_CAT");
                list.add(listofDatabases);
            }
        } catch (ClassNotFoundException e) {
            System.out.println("Erro de CNF ao buscar o banco de dados.");
            e.printStackTrace();
        } catch (SQLException se) {
            System.out.println("Erro de SQL ao buscar o banco de dados.");
            se.printStackTrace();
        }
        if (list.contains(nmDatabase)) {
            return true;
        }
        return false;
    }
}

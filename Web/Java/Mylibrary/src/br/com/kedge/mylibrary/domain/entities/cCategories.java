package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cCategories extends cEntityDomain {

    private String name;

    cCategories() {
    }

    cCategories(int id, boolean active, String name) {
        super(id, active);

        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

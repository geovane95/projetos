package br.com.kedge.mylibrary.core.implementation.business;

import br.com.kedge.mylibrary.core.iStrategy;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entities.cAddress;

public class cValidateRequiredInformationAddress implements iStrategy {
    @Override
    public String process(cEntityDomain entity) {

        if (entity instanceof cAddress) {
            cAddress objAddress = (cAddress) entity;

            String number = objAddress.getNumber();

            if (number == null) {
                return "O nome do nivel de acesso é obrigatório!";
            }

            if (number.trim().equals("")) {
                return "O nome do nivel de acesso é obrigatório!";
            }
        } else {
            return "Deve ser registrado um Nivel de acesso!";
        }

        return null;
    }
}

package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cCity extends cEntityDomain {

    private String city;

    private cState objState;

    public cCity() {
    }

    public cCity(int id, boolean active, String city, cState objState) {
        super(id, active);

        this.setCity(city);
        this.setObjState(objState);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public cState getObjState() {
        return objState;
    }

    public void setObjState(cState objState) {
        this.objState = objState;
    }
}

package br.com.kedge.mylibrary.domain.entities;

public class cStockParameter {

    private int minNumSto;

    private int maxNumSto;

    public cStockParameter() {
    }

    public cStockParameter(int minNumSto, int maxNumSto) {
        this.minNumSto = minNumSto;
        this.maxNumSto = maxNumSto;
    }
}

package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cPhone extends cEntityDomain {

    private String phone;

    private cTypePhone objTypePhone;

    public cPhone(int id, boolean active, String phone, cTypePhone objTypePhone) {
        super(id, active);

        this.setPhone(phone);
        this.setObjTypePhone(objTypePhone);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public cTypePhone getObjTypePhone() {
        return objTypePhone;
    }

    public void setObjTypePhone(cTypePhone objTypePhone) {
        this.objTypePhone = objTypePhone;
    }
}

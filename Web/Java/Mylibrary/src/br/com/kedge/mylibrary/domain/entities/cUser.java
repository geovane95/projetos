package br.com.kedge.mylibrary.domain.entities;

import java.util.Date;

public class cUser extends acPerson {

    private String email;

    private String password;

    private Date dtRegister;

    private boolean active;

    private cAccessLevel objAccessLevel;

    public cUser() {
    }

    public cUser(int id, boolean active, String name, Date dtBeginLife, String email, String password,
                 Date dtRegister, cAccessLevel objAccessLevel) {
        super(id, active, name, dtBeginLife);

        this.setEmail(email);

        this.setPassword(password);

        this.setDtRegister(dtRegister);

        this.setObjAccessLevel(objAccessLevel);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDtRegister() {
        return dtRegister;
    }

    public void setDtRegister(Date dtRegister) {
        this.dtRegister = dtRegister;
    }

    public cAccessLevel getObjAccessLevel() {
        return objAccessLevel;
    }

    public void setObjAccessLevel(cAccessLevel objAccessLevel) {
        this.objAccessLevel = objAccessLevel;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }
}

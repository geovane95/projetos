package br.com.kedge.mylibrary.domain.entities;

import java.util.Date;

public class cAuthor extends acPerson {

    private int books;

    public cAuthor() {
    }

    public cAuthor(int id, boolean active, String name, Date dtBeginLife, int books) {
        super(id, active, name, dtBeginLife);

        this.setBooks(books);
    }

    public int getBooks() {
        return books;
    }

    public void setBooks(int books) {
        this.books = books;
    }
}

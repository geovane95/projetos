package br.com.kedge.mylibrary.domain.entities;

public class cAddressLP {

    private cLegalPerson objLegalPerson;

    private cAddress objAddress;

    public cAddressLP() {

    }

    public cAddressLP(cLegalPerson objLegalPerson, cAddress objAddress) {

        this.setObjLegalPerson(objLegalPerson);
        this.setObjAddress(objAddress);
    }

    public cLegalPerson getObjLegalPerson() {
        return objLegalPerson;
    }

    public void setObjLegalPerson(cLegalPerson objLegalPerson) {
        this.objLegalPerson = objLegalPerson;
    }

    public cAddress getObjAddress() {
        return objAddress;
    }

    public void setObjAddress(cAddress objAddress) {
        this.objAddress = objAddress;
    }
}

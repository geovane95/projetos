package br.com.kedge.mylibrary.web.viewhelper.implementation;

import br.com.kedge.mylibrary.core.aplication.cResult;
import br.com.kedge.mylibrary.core.util.cConvertDate;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entities.cAuthor;
import br.com.kedge.mylibrary.web.viewhelper.iViewHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class cAuthorViewHelper implements iViewHelper {

    public cEntityDomain getEntity(HttpServletRequest request) {
        String operation = request.getParameter("operation");
        cAuthor objAuthor = null;

        if (!operation.equals("Visualizar")) {
            String id = request.getParameter("txtId");
            String name = request.getParameter("txtName");
            String dtBirth = request.getParameter("txtdtBirth");
            String books = request.getParameter("txtBooks");
            int iBooks = 0;
            boolean active = Boolean.parseBoolean(request.getParameter("txtActive"));

            objAuthor = new cAuthor();

            if (id != null && !id.trim().equals("")) {
                objAuthor.setId(Integer.parseInt(id));
            }
            if (name != null && !name.trim().equals("")) {
                objAuthor.setName(name);
            }
            if (dtBirth != null && !dtBirth.trim().equals("")) {
                objAuthor.setDtBeginLife(cConvertDate.convertStringDate(dtBirth));
            }
            if (books != null && !books.trim().equals("")) {
                iBooks = Integer.parseInt(books);
                objAuthor.setBooks(iBooks);
            }
            if (active) {
                objAuthor.setActive(true);
            } else {
                objAuthor.setActive(false);
            }
        } else {
            HttpSession session = request.getSession();
            cResult objResult = (cResult) session.getAttribute("objresult");
            String txtId = request.getParameter("txtId");
            int id = 0;

            if (txtId != null && !txtId.trim().equals("")) {
                id = Integer.parseInt(txtId);
            }

            for (cEntityDomain objED : objResult.getEntities()) {
                if (objED.getId() == id) {
                    objAuthor = (cAuthor) objED;
                }
            }
        }

        return objAuthor;
    }

    public void setView(cResult objResult, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        RequestDispatcher rd = null;

        String operation = request.getParameter("operation");

        if (objResult.getMsg() == null) {
            if (operation.equals("Salvar")) {
                objResult.setMsg("Autor inserido com sucesso!");
            }

            request.getSession().setAttribute("objresult", objResult);
            rd = request.getRequestDispatcher("ConsultarAutor.jsp");
        }

        if (objResult.getMsg() == null && operation.equals("Alterar")) {
            rd = request.getRequestDispatcher("ConsultarAutor.jsp");
        }
        if (objResult.getMsg() == null && operation.equals("Visualizar")) {
            request.setAttribute("objauthor", objResult.getEntities().get(0));
            rd = request.getRequestDispatcher("Autor.jsp");
        }

        if (objResult.getMsg() == null && operation.equals("Excluir")) {
            request.getSession().setAttribute("objresult", null);
            rd = request.getRequestDispatcher("ConsultarAutor.jsp");
        }

        if (objResult.getMsg() != null) {
            if (operation.equals("Salvar") || operation.equals("Alterar")) {
                request.getSession().setAttribute("objresult", objResult);
                rd = request.getRequestDispatcher("ConsultarAutor.jsp");
            }
        }

        if (objResult.getMsg() == null && operation.equals("Ativar")) {
            rd = request.getRequestDispatcher("ConsultarAutor.jsp");
        }

        if (objResult.getMsg() == null && operation.equals("Inativar")) {
            request.getSession().setAttribute("objresult", null);
            rd = request.getRequestDispatcher("ConsultarAutor.jsp");
        }

        rd.forward(request, response);
    }
}

package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cPricingGroup extends cEntityDomain {

    private cPricing objPricing;

    public cPricingGroup() {
    }

    public cPricingGroup(int id, boolean active, cPricing objPricing) {
        super(id, active);

        this.setObjPricing(objPricing);
    }

    public cPricing getObjPricing() {
        return objPricing;
    }

    public void setObjPricing(cPricing objPricing) {
        this.objPricing = objPricing;
    }
}

package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cAddress extends cEntityDomain {

    private String number;

    private String comment;

    private cTypeHouse objTypeHouse;

    private cLegalPerson objLegalPerson;

    private cNaturalPerson objNatualPerson;

    private cLocation objLocation;

    private boolean preferential;

    public cAddress(int id, boolean active, String number, cTypeHouse objTypeHouse,
                    cLocation objLocation, boolean preferential) {
        super(id, active);

        this.setNumber(number);
        this.setObjTypeHouse(objTypeHouse);
        this.setObjLocation(objLocation);
        this.setPreferential(preferential);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public cTypeHouse getObjTypeHouse() {
        return objTypeHouse;
    }

    public void setObjTypeHouse(cTypeHouse objTypeHouse) {
        this.objTypeHouse = objTypeHouse;
    }

    public cLocation getObjLocation() {
        return objLocation;
    }

    public void setObjLocation(cLocation objLocation) {
        this.objLocation = objLocation;
    }

    public boolean isPreferential() {
        return preferential;
    }

    public void setPreferential(boolean preferential) {
        this.preferential = preferential;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public cLegalPerson getObjLegalPerson() {
        return objLegalPerson;
    }

    public void setObjLegalPerson(cLegalPerson objLegalPerson) {
        this.objLegalPerson = objLegalPerson;
    }

    public cNaturalPerson getObjNatualPerson() {
        return objNatualPerson;
    }

    public void setObjNatualPerson(cNaturalPerson objNatualPerson) {
        this.objNatualPerson = objNatualPerson;
    }
}

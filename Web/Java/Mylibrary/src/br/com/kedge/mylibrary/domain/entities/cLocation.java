package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cLocation extends cEntityDomain {

    private String zipcode;

    private cStreet objStreet;

    private String neighborhood;

    private cCity objCity;

    public cLocation(int id, boolean active, String zipcode, cStreet objStreet,
                     String neighborhood, cCity objCity) {
        super(id, active);

        this.setZipcode(zipcode);
        this.setObjStreet(objStreet);
        this.setNeighborhood(neighborhood);
        this.setObjCity(objCity);
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public cStreet getObjStreet() {
        return objStreet;
    }

    public void setObjStreet(cStreet objStreet) {
        this.objStreet = objStreet;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public cCity getObjCity() {
        return objCity;
    }

    public void setObjCity(cCity objCity) {
        this.objCity = objCity;
    }
}

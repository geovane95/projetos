package br.com.kedge.mylibrary.domain.entities;

import java.util.Date;

public class cLegalPerson extends acCustomer {

    private String cnpj;

    private String StateRegister;

    private String MunicipalRegister;

    private String companyName;

    cLegalPerson() {
    }

    cLegalPerson(int id, boolean active, String name, Date dtBeginLife, String email,
                 String password, Date dtRegister, cAccessLevel objAccessLevel,
                 cPhone objPhones, cAddress objAddress, String cnpj, String StateRegister,
                 String MunicipalRegister, String companyName) {
        super(id, active, name, dtBeginLife, email, password, dtRegister,
                objAccessLevel, objPhones, objAddress);

        this.setCnpj(cnpj);

        this.setStateRegister(StateRegister);

        this.setMunicipalRegister(MunicipalRegister);

        this.setCompanyName(companyName);
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getStateRegister() {
        return StateRegister;
    }

    public void setStateRegister(String stateRegister) {
        StateRegister = stateRegister;
    }

    public String getMunicipalRegister() {
        return MunicipalRegister;
    }

    public void setMunicipalRegister(String municipalRegister) {
        MunicipalRegister = municipalRegister;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}

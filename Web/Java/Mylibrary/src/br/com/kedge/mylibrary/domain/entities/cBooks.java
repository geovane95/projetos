package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

import java.util.Date;
import java.util.List;

public class cBooks extends cEntityDomain {

    private String title;

    private String synopsis;

    private Date dtRegister;

    private List<cCategories> categories;

    private cAuthor objAuthor;

    public cBooks() {
    }

    public cBooks(int id, boolean active, String title, String synopsis, Date dtRegister, List<cCategories> categories, cAuthor objAuthor) {
        super(id, active);

        this.setTitle(title);
        this.setSynopsis(synopsis);
        this.setCategories(categories);
        this.setObjAuthor(objAuthor);
        this.setDtRegister(dtRegister);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public List<cCategories> getCategories() {
        return categories;
    }

    public void setCategories(List<cCategories> categories) {
        this.categories = categories;
    }

    public cAuthor getObjAuthor() {
        return objAuthor;
    }

    public void setObjAuthor(cAuthor objAuthor) {
        this.objAuthor = objAuthor;
    }

    public Date getDtRegister() {
        return dtRegister;
    }

    public void setDtRegister(Date dtRegister) {
        this.dtRegister = dtRegister;
    }
}

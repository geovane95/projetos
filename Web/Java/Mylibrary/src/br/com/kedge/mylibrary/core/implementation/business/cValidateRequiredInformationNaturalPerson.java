package br.com.kedge.mylibrary.core.implementation.business;

import br.com.kedge.mylibrary.core.iStrategy;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entities.cNaturalPerson;

import java.util.Date;

public class cValidateRequiredInformationNaturalPerson implements iStrategy {
    @Override
    public String process(cEntityDomain entity) {

        if (entity instanceof cNaturalPerson) {
            cNaturalPerson objNaturalPerson = (cNaturalPerson) entity;

            String name = objNaturalPerson.getName();
            String cpf = objNaturalPerson.getCpf();
            String rg = objNaturalPerson.getGeneralRegister();
            String email = objNaturalPerson.getEmail();
            String senha = objNaturalPerson.getPassword();
            Date dtNasc = objNaturalPerson.getDtBeginLife();

            if (name == null || name.trim().equals("")) {
                return "O nome do cliente é obrigatório!";
            }

            if (cpf == null || cpf.trim().equals("")) {
                return "O CPF do cliente é obrigatório!";
            }

            if (rg == null || rg.trim().equals("")) {
                return "O RG do cliente é obrigatório!";
            }

            if (email == null || email.trim().equals("")) {
                return "O email do cliente é obrigatório!";
            }

            if (senha == null || senha.trim().equals("")) {
                return "A senha do cliente é obrigatório!";
            }

            if (dtNasc == null || dtNasc.getTime() == 0) {
                return "A data de nascimento deve ser preenchida";
            }

        } else {
            return "Deve ser registrado um Cliente!";
        }

        return null;
    }
}

package br.com.kedge.mylibrary.core.implementation.dao;

import br.com.kedge.mylibrary.core.util.acTableModels;
import br.com.kedge.mylibrary.core.util.cConvertDate;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entities.acPerson;
import br.com.kedge.mylibrary.domain.entities.cAuthor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class cAuthorsDao extends acJdbcDao {

    public cAuthorsDao() {
        super(acTableModels.author[0], acTableModels.author[1]);
    }

    public void create(cEntityDomain objEntityDomain) {
        openConnection();
        PreparedStatement pst = null;
        cAuthor objAuthor = (cAuthor) objEntityDomain;

        try {
            connection.setAutoCommit(false);
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO authors (active, firstname, middlename, lastname, dtbirth, books) ");
            sql.append("VALUES (?,?,?,?,?,?)");

            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
            pst.setBoolean(1, objAuthor.isActive());
            pst.setString(2, acPerson.getFirstname(objAuthor.getName()));
            pst.setString(3, acPerson.getMiddlename(objAuthor.getName()));
            pst.setString(4, acPerson.getLastname(objAuthor.getName()));
            pst.setDate(5, cConvertDate.convertStringSqlDate(objAuthor.getDtBeginLife()));
            pst.setInt(6, objAuthor.getBooks());

            ResultSet rs = pst.getGeneratedKeys();
            if (rs.next()) {
                objAuthor.setId(rs.getInt(1));
            }

            pst.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException se) {
                se.printStackTrace();
                System.out.println("Erro de SQL ao tentar efetuar o rollback!");
            }
            e.printStackTrace();
            System.out.println("Erro ao executar a inserção de um novo Autor!");
        } finally {
            closeConnection(pst, true);
        }
    }

    @Override
    public void update(cEntityDomain objEntityDomain) {
        openConnection();
        PreparedStatement pst = null;
        cAuthor objAuthor = (cAuthor) objEntityDomain;

        try {
            connection.setAutoCommit(false);

            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE authors SET active=?, firstname=?, middlename=?, lastname=?, dtbirth=?, books=? ");
            sql.append("WHERE id=?");

            pst = connection.prepareStatement(sql.toString());

            pst.setBoolean(1, objAuthor.isActive());
            pst.setString(2, acPerson.getFirstname(objAuthor.getName()));
            pst.setString(3, acPerson.getMiddlename(objAuthor.getName()));
            pst.setString(4, acPerson.getLastname(objAuthor.getName()));
            pst.setDate(5, cConvertDate.convertStringSqlDate(objAuthor.getDtBeginLife()));
            pst.setInt(6, objAuthor.getBooks());
            pst.setInt(7, objAuthor.getId());

            pst.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException se) {
                se.printStackTrace();
                System.out.println("Erro ao tentar executar o rollback!");
            }
            e.printStackTrace();
            System.out.println("Erro ao tentar alterar o registro de Id: " +
                    objAuthor.getId() + " da Tabela Autores!");
        } finally {
            closeConnection(pst, true);
        }
    }

    @Override
    public List<cEntityDomain> retrive(cEntityDomain objEntityDomain) {

        PreparedStatement pst = null;

        cAuthor objAuthor = (cAuthor) objEntityDomain;

        String sql = null;

        if (objAuthor.getName() == null) {
            objAuthor.setName("");
        }

        if (objAuthor.getId() == null && objAuthor.getName().equals("")) {
            sql = "SELECT * FROM authors";
        } else if (objAuthor.getId() != null && objAuthor.getName().equals("")) {
            sql = "SELECT * FROM authors WHERE id=?";
        } else if (objAuthor.getId() == null && !objAuthor.getName().equals("")) {
            sql = "SELECT * FROM authors WHERE firstname like ?";
        }

        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            if (objAuthor.getId() != null && objAuthor.getName().equals("")) {
                pst.setInt(1, objAuthor.getId());
            } else if (objAuthor.getId() == null && !objAuthor.getName().equals("")) {
                pst.setString(1, "%" + objAuthor.getName() + "%");
            }

            ResultSet rs = pst.executeQuery();

            List<cEntityDomain> objAuthors = new ArrayList<cEntityDomain>();
            while (rs.next()) {
                cAuthor ObjAuthor = new cAuthor();
                ObjAuthor.setId(rs.getInt("id"));
                ObjAuthor.setName(acPerson.getAllName(rs.getString("firstname"), rs.getString("middlename"), rs.getString("lastname")));
                ObjAuthor.setDtBeginLife(rs.getDate("dtbirth"));
                ObjAuthor.setBooks(rs.getInt("books"));
                ObjAuthor.setActive(rs.getBoolean("active"));

                objAuthors.add(ObjAuthor);
            }
            return objAuthors;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Erro ao consultar a tabela de Autores!");
        }
        return null;
    }
}

package br.com.kedge.mylibrary.domain.entities;

public class cPricing {

    private double margem_lucro;

    cPricing() {
    }

    cPricing(double margem_lucro) {
        this.setMargem_lucro(margem_lucro);
    }

    public double getMargem_lucro() {
        return margem_lucro;
    }

    public void setMargem_lucro(double margem_lucro) {
        this.margem_lucro = margem_lucro;
    }
}

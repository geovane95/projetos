package br.com.kedge.mylibrary.domain.entities;

import java.util.Date;

public class cEditors extends cLegalPerson {

    private int books;

    cEditors() {
    }

    cEditors(int id, boolean active, String name, Date dtBeginLife, String email,
             String password, Date dtRegister, cAccessLevel objAccessLevel,
             cPhone objPhones, cAddress objAddress, String cnpj, String StateRegister,
             String MunicipalRegister, String companyName, int books) {
        super(id, active, name, dtBeginLife, email, password, dtRegister, objAccessLevel,
                objPhones, objAddress, cnpj, StateRegister, MunicipalRegister, companyName);

        this.setBooks(books);
    }

    public int getBooks() {
        return books;
    }

    public void setBooks(int books) {
        this.books = books;
    }
}

package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

import java.util.Date;

public class acPerson extends cEntityDomain {

    private String name;

    private Date dtBeginLife;

    public acPerson() {

    }

    public acPerson(int id, boolean active, String name, Date dtBeginLife) {
        super(id, active);

        this.setName(name);
        this.setDtBeginLife(dtBeginLife);
    }

    public static String getFirstname(String name) {
        return name.split(" ")[0];
    }

    public static String getMiddlename(String name) {
        StringBuilder middlename = new StringBuilder();
        for (String st : name.split(" ")) {
            if (st.equals(name.split(" ")[0])) {
            } else if (st.equals(name.split(" ")[(name.split(" ").length) - 2])) {
                middlename.append(st);
            } else if (st.equals(name.split(" ")[(name.split(" ").length) - 1])) {
            } else {
                middlename.append(st).append(" ");
            }
        }
        return middlename.toString();
    }

    public static String getLastname(String name) {
        return name.split(" ")[(name.split(" ").length) - 1];
    }

    public static String getAllName(String firstname, String middlename, String lastname) {
        return firstname + " " + middlename + " " + lastname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDtBeginLife() {
        return dtBeginLife;
    }

    public void setDtBeginLife(Date dtBeginLife) {
        this.dtBeginLife = dtBeginLife;
    }
}

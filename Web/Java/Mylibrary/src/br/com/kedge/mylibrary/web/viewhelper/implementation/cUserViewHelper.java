package br.com.kedge.mylibrary.web.viewhelper.implementation;

import br.com.kedge.mylibrary.core.aplication.cResult;
import br.com.kedge.mylibrary.core.implementation.dao.cAccessLevelDao;
import br.com.kedge.mylibrary.domain.cEntityDomain;
import br.com.kedge.mylibrary.domain.entities.cUser;
import br.com.kedge.mylibrary.web.viewhelper.iViewHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class cUserViewHelper implements iViewHelper {

    public cEntityDomain getEntity(HttpServletRequest request) {

        String operation = request.getParameter("operation");
        cUser objUser = null;
        cAccessLevelDao objAccessLevelDao = new cAccessLevelDao();

        if (!operation.equals("Visulizar")) {
            String id = request.getParameter("txtId");
            String email = request.getParameter("txtEmail");
            String senha = request.getParameter("txtSenha");
            String access = request.getParameter("txtAccess");
            int iAccess = 0;
            String active = request.getParameter("txtActive");

            objUser = new cUser();

            if (id != null && !id.trim().equals("")) {
                objUser.setId(Integer.parseInt(id));
            }
            if (email != null && !email.trim().equals("")) {
                objUser.setEmail(email);
            }
            if (senha != null && !senha.trim().equals("")) {
                objUser.setPassword(senha);
            }
            if (active != null && active.trim().equals("")) {
                if (active == "true") {
                    objUser.setActive(true);
                } else {
                    objUser.setActive(false);
                }
            }
            if (access != null && !access.trim().equals("")) {
                iAccess = Integer.parseInt(access);
            }
            objUser.setObjAccessLevel(objAccessLevelDao.getAccessLevelById(iAccess));
        } else {
            HttpSession session = request.getSession();
            cResult objResult = (cResult) session.getAttribute("objresult");
            String txtId = request.getParameter("txtId");
            int id = 0;

            if (txtId != null && !txtId.trim().equals("")) {
                id = Integer.parseInt(txtId);
            }

            for (cEntityDomain objED : objResult.getEntities()) {
                if (objED.getId() == id) {
                    objUser = (cUser) objED;
                }
            }
        }

        return objUser;
    }

    public void setView(cResult objResult, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        RequestDispatcher rd = null;

        String operation = request.getParameter("operation");

        if (objResult.getMsg() == null) {
            if (operation.equals("Salvar")) {
                objResult.setMsg("Usuario inserido com sucesso!");
            }

            request.getSession().setAttribute("objresult", objResult);
            rd = request.getRequestDispatcher("ConsultaUsuario.jsp");
        }

        if (objResult.getMsg() == null && operation.equals("Alterar")) {
            rd = request.getRequestDispatcher("ConsultaUsuario.jsp");
        }

        if (objResult.getMsg() == null && operation.equals("Visualizar")) {
            request.setAttribute("objuser", objResult.getEntities().get(0));
            rd = request.getRequestDispatcher("Usuario.jsp");
        }

        if (objResult.getMsg() == null && operation.equals("Excluir")) {
            request.getSession().setAttribute("objresult", null);
            rd = request.getRequestDispatcher("ConsultaUsuario.jsp");
        }

        if (objResult.getMsg() != null) {
            if (operation.equals("Salvar") || operation.equals("Alterar")) {
                request.getSession().setAttribute("objresult", objResult);
                rd = request.getRequestDispatcher("ConsultaUsuario.jsp");
            }
        }

        if (objResult.getMsg() == null && operation.equals("Ativar")) {
            rd = request.getRequestDispatcher("ListaUsuarios.jsp");
        }

        if (objResult.getMsg() == null && operation.equals("Inativar")) {
            rd = request.getRequestDispatcher("ListaUsuarios.jsp");
        }

        System.out.println(rd.toString());
        rd.forward(request, response);
    }
}

package br.com.kedge.mylibrary.domain.entities;

import br.com.kedge.mylibrary.domain.cEntityDomain;

public class cState extends cEntityDomain {

    private String state;

    private cCountry objCountry;

    public cState() {
    }

    public cState(int id, boolean active, String state, cCountry objCountry) {
        super(id, active);

        this.setState(state);
        this.setObjCountry(objCountry);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public cCountry getObjCountry() {
        return objCountry;
    }

    public void setObjCountry(cCountry objCountry) {
        this.objCountry = objCountry;
    }
}

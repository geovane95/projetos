package br.com.kedge.mylibrary.domain.entities;

public class cAddressNP {

    private cNaturalPerson objNaturalPerson;

    private cAddress objAddress;

    public cAddressNP() {

    }

    public cAddressNP(cNaturalPerson objNaturalPerson, cAddress objAddress) {

        this.setObjNaturalPerson(objNaturalPerson);
        this.setObjAddress(objAddress);
    }

    public cAddress getObjAddress() {
        return objAddress;
    }

    public void setObjAddress(cAddress objAddress) {
        this.objAddress = objAddress;
    }

    public cNaturalPerson getObjNaturalPerson() {
        return objNaturalPerson;
    }

    public void setObjNaturalPerson(cNaturalPerson objNaturalPerson) {
        this.objNaturalPerson = objNaturalPerson;
    }
}
